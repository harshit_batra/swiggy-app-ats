all: setup

setup:
	export testenv=`echo $environment | tr '[:lower:]' '[:upper:]'`
	echo $testenv
	echo "`cat /etc/hosts`\twiremock" > /etc/hosts
