package com.swiggy.dineoutUI.assertions.consumer;

import com.swiggy.automation.ui.assertions.AssertChains;
import com.swiggy.automation.ui.listeners.DriverCreationListener;
import com.swiggy.dineoutUI.pages.consumer.OrderConfirmation.OrderConfirmationPage;
import com.swiggy.dineoutUI.pages.consumer.home.ConsumerHomePage;

public class ConsumerOrderConfirmationPageAssertions {

    public AssertChains assertChains = new AssertChains(DriverCreationListener.driver.get());

    public void orderConfirmationAssertions(OrderConfirmationPage orderConfirmationPage) {
        assertChains.element(orderConfirmationPage.OUTLET_DETAILS)
                .isPresent(10, "Outlet Details Not Visible After Placing Order")
                .end();
    }
}
