package com.swiggy.dineoutUI.assertions.consumer;

import com.swiggy.automation.ui.assertions.AssertChains;
import com.swiggy.automation.ui.listeners.DriverCreationListener;
import com.swiggy.dineoutUI.pages.consumer.home.ConsumerHomePage;
import com.swiggy.dineoutUI.pages.vendor.home.VendorHomePage;

public class ConsumerHomePageAssertions {

    public AssertChains assertChains = new AssertChains(DriverCreationListener.driver.get());

    public void consumerHomePageAssertions(ConsumerHomePage consumerHomePage) {
        assertChains.element(consumerHomePage.SWIGGY_ICON)
                .isPresent("Swiggy Icon Is Not Present")
                .then()
                .element(consumerHomePage.SEARCH_ICON)
                .isPresent("Search Icon Is Not Present")
                .then()
                .element(consumerHomePage.CART_ICON)
                .isPresent("Cart Icon Is Not Present")
                .then()
                .element(consumerHomePage.ACCOUNT_ICON)
                .isPresent("Account Icon Is Not Present")
                .then()
                .element(consumerHomePage.LOCATION_DROPDOWN_ICON)
                .isPresent(10, "Location Dropdown Is Not Present")
                .end();
    }

    public void consumerHomePageAssertionsAfterSelectingLocation(ConsumerHomePage consumerHomePage) {
        assertChains.element(consumerHomePage.ALL_RESTAURANTS)
                .isPresent(10, "All Restaurants Card Is Not Visible")
                .end();
    }
}
