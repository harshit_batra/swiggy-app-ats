package com.swiggy.dineoutUI.assertions.vendor;

import com.swiggy.automation.ui.assertions.AssertChains;
import com.swiggy.automation.ui.listeners.DriverCreationListener;
import com.swiggy.dineoutUI.pages.vendor.home.VendorHomePage;

public class VendorHomePageAssertions {

    public AssertChains assertChains = new AssertChains(DriverCreationListener.driver.get());

    public void vendorHomePageAssertions(VendorHomePage vendorHomePage) {
        assertChains.element(vendorHomePage.ORDERS_TAB)
                .isPresent("Orders Tab Is Not Visible")
                .then()
                .element(vendorHomePage.INVENTORY_TAB)
                .isPresent("Inventory Tab Is Not Visible")
                .then()
                .element(vendorHomePage.REPORTS_TAB)
                .isPresent("Reports Tab Is Not Visible")
                .then()
                .element(vendorHomePage.MORE_TAB)
                .isPresent("More Tab Is Not Present")
                .end();
    }
}
