package com.swiggy.dineoutUI.widgets;

import com.swiggy.automation.ui.locators.LocatorType;
import com.swiggy.dineoutUI.pages.SwiggyDineoutBasePage;
import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;

public class LogoutConfirm extends SwiggyDineoutBasePage<LogoutConfirm> {

    public By LOGOUT_CONFIRM = getLocator("LOGOUT_CONFIRM");

    @Override
    protected void setLocators() {
        super.setLocators();
       // setLocator("ALLOW_BUTTON", LocatorType.ANDROID_LOCATOR,By.xpath("//android.widget.Button[contains(@resource-id,'permission_allow_button')]"));

        setLocator("LOGOUT_CONFIRM", LocatorType.ANDROID_LOCATOR, By.id("dialog_positive_layout_text"));

    }

    public LogoutConfirm waitForDialogToBeDisplayed() {
        actions.element().isElementDisplayed(LOGOUT_CONFIRM, 5);
        return this;
    }

    public LogoutConfirm clickOnConfirmLogoutButton() {
        actions.element().click(LOGOUT_CONFIRM);
        return this;
    }
}
