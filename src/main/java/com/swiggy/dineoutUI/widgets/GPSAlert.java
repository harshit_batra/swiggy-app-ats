package com.swiggy.dineoutUI.widgets;

import com.swiggy.automation.ui.enums.Platform;
import com.swiggy.automation.ui.helpers.CommonHelpers;
import com.swiggy.automation.ui.locators.LocatorType;
import com.swiggy.dineoutUI.pages.SwiggyDineoutBasePage;
import com.swiggy.dineoutUI.pages.consumer.home.ConsumerHomePage;
import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;
//import org.tinylog.Logger;

public class GPSAlert extends SwiggyDineoutBasePage<GPSAlert> {

    public static final String ALLOW_GPS_ALERT_TEXT = getGPSText();
  //  public static final String GOOGLE_LOCATION_SERVICE_TEXT = "For a better experience, turn on device location, which uses Google’s location service.";
    public By ALLOW_BUTTON = getLocator("ALLOW_BUTTON");
    public By DENY_BUTTON = getLocator("DENY_BUTTON");
    public By ALLOW_NOTIFICATIONS= getLocator("ALLOW_NOTIFICATIONS");
    public By GPS_LOCATION_POP_UP_OK_BTN= getLocator("GPS_LOCATION_POP_UP_OK_BTN");
    public By CONFIRM_LOCATION = getLocator("CONFIRM_LOCATION");

    public static String getGPSText() {
        return "to access this device's location?";
    }

    @Override
    protected void setLocators() {
        super.setLocators();
        setLocator("ALLOW_BUTTON", LocatorType.ANDROID_LOCATOR,By.xpath("//android.widget.Button[contains(@resource-id,'permission_allow_button')]"));
        setLocator("ALLOW_BUTTON", LocatorType.IOS_LOCATOR, By.name("Allow While Using App"));

        setLocator("ALLOW_NOTIFICATIONS", LocatorType.IOS_LOCATOR, MobileBy.AccessibilityId("Allow"));

        setLocator("DENY_BUTTON" , LocatorType.IOS_LOCATOR, By.xpath("//XCUIElementTypeButton[contains(@name,'Don') and contains(@name,'Allow')]"));
        setLocator("DENY_BUTTON", LocatorType.ANDROID_LOCATOR,By.xpath("//android.widget.Button[contains(@resource-id,'permission_deny')]"));

        setLocator("GPS_LOCATION_POP_UP_OK_BTN", LocatorType.ANDROID_LOCATOR,By.xpath("//android.widget.Button[@text='OK']"));

        setLocator("CONFIRM_LOCATION", LocatorType.ANDROID_LOCATOR, By.id("google_place_search_title_text1"));
        setLocator("CONFIRM_LOCATION", LocatorType.IOS_LOCATOR, By.name("CONFIRM LOCATION"));
    }

    public ConsumerHomePage allowGoogleLocationService() {
        if(CommonHelpers.getCurrentPlatform().equals(Platform.ANDROID)){
            if(actions.element().isElementDisplayed(GPS_LOCATION_POP_UP_OK_BTN,2))
            {
                actions.element().click(GPS_LOCATION_POP_UP_OK_BTN);
            }
        }
        return ConsumerHomePage.getConsumerHomePage();
    }


    public GPSAlert allowNotifications(){
        if(CommonHelpers.getCurrentPlatform().equals(Platform.IOS)
                && actions.element().isElementPresent(ALLOW_NOTIFICATIONS,5)) {
            actions.element().click(ALLOW_NOTIFICATIONS);
            return this;
        }
        return this;
    }

    public GPSAlert allowGPS() {

        if (CommonHelpers.getCurrentPlatform().equals(Platform.IOS) || CommonHelpers.getCurrentOsVersion().contains("10.")) {
            actions.element().click(ALLOW_BUTTON);
            return this;
        } else {
            if(actions.element().isElementDisplayed(ALLOW_BUTTON)){
                actions.element().click(ALLOW_BUTTON);
            } else {
                actions.alert().accept(ALLOW_GPS_ALERT_TEXT);
            }
        }
        return this;
    }

    public GPSAlert denyGPS() {

        if (CommonHelpers.getCurrentPlatform().equals(Platform.IOS) || CommonHelpers.getCurrentOsVersion().contains("10.")) {
            actions.element().click(DENY_BUTTON);
            return this;
        } else {
            if(actions.element().isElementDisplayed(DENY_BUTTON)){
                actions.element().click(DENY_BUTTON);
            } else {
                actions.alert().dismiss(ALLOW_GPS_ALERT_TEXT);
            }
        }
        return this;
    }

//    public GPSAlert denyGPS() {
//        SwiggyDineoutBasePage.getConsumerAppsFlowData().getGpsPopup().setGPSDenied(true);
//        Logger.debug("GPSAlert status: {}", SwiggyDineoutBasePage.getConsumerAppsFlowData().getGpsPopup().isGPSDenied());
//        if (actions.element().isElementPresent(DENY_BUTTON,5)) {
//            actions.element().click(DENY_BUTTON);
//            return this;
//        }
//        actions.alert().dismiss(ALLOW_GPS_ALERT_TEXT);
//        return this;
//    }
//
//    public GPSAlert confirmLocation() {
//        actions.element().doesNotHasText(CONFIRM_LOCATION,"Locating",true);
//        actions.element().hasText(CONFIRM_LOCATION,",",true);
//        String attribute="clickable";
//        if(CommonHelpers.getCurrentPlatform().equals(Platform.IOS)){
//            attribute="enabled";
//        }
//        actions.element().click(CONFIRM_LOCATION,attribute,"true");
//        SwiggyDineoutBasePage.getConsumerAppsFlowData().getLocationPopUp().setLocationChanged(true);
//        if(SwiggyDineoutBasePage.getConsumerAppsFlowData().getGpsPopup().isGPSDenied()){
//            return new GPSAlert();
//        }
//        return new GPSAlert();
//    }


}
