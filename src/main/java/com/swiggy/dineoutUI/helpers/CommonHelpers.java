package com.swiggy.dineoutUI.helpers;

import com.swiggy.automation.ui.listeners.DriverCreationListener;
import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;
import org.tinylog.Logger;

import java.time.Duration;

public class CommonHelpers{

    static AndroidDriver driver = (AndroidDriver) DriverCreationListener.driver.get();

    public static void openAnotherApp(String packageName, String activityName, String appWaitPackage, String appWaitActivity, boolean setStopApp) {
        Logger.info("Starting activity With Package " + packageName + " and Activity " + activityName);
        Activity activity = new Activity(packageName, activityName);
        if(!appWaitPackage.equals("")) {
            activity.setAppWaitPackage(appWaitPackage);
        }
        if(!appWaitActivity.equals("")) {
            activity.setAppWaitActivity(appWaitActivity);
        }
        activity.setStopApp(setStopApp);
        driver.startActivity(activity);
    }

    public static void putAppInBackground() {
        driver.runAppInBackground(Duration.ofSeconds(600));
    }

    public static void activateBackgroundApp(String packageName) {
        driver.activateApp(packageName);
    }
}
