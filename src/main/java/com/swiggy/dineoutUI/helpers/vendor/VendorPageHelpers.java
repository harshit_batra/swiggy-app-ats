package com.swiggy.dineoutUI.helpers.vendor;

import com.swiggy.dineoutUI.pages.vendor.home.VendorHomePage;
import com.swiggy.dineoutUI.pages.vendor.login.RestaurantLoginPage;

public class VendorPageHelpers {

    public VendorHomePage loginToVendorApp(String mobileOrId, String password) {
        RestaurantLoginPage restaurantLoginPage = RestaurantLoginPage.getRestaurantLoginPage();
        return restaurantLoginPage
                .enterMobileNumberOrId(mobileOrId)
                .clickOnContinueButton()
                .enterPasswordOrOtp(password)
                .clickOnLoginButton();
    }
}
