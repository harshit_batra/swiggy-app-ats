package com.swiggy.dineoutUI.helpers.consumer;

import com.swiggy.dineoutUI.pages.consumer.Cart.CartPage;
import com.swiggy.dineoutUI.pages.consumer.OrderConfirmation.OrderConfirmationPage;
import com.swiggy.dineoutUI.pages.consumer.RestaurantDetail.RestaurantDetailPage;
import com.swiggy.dineoutUI.pages.consumer.checkout.CheckoutPage;
import com.swiggy.dineoutUI.pages.consumer.home.ConsumerHomePage;
import com.swiggy.dineoutUI.pages.consumer.login.ConsumerLoginPage;
import com.swiggy.dineoutUI.pages.consumer.loginInfo.LoginInfoPage;
import com.swiggy.dineoutUI.pages.consumer.otp.OtpPage;
import com.swiggy.dineoutUI.pages.consumer.restaurants.RestaurantsPage;
import com.swiggy.dineoutUI.widgets.GPSAlert;

public class ConsumerPageHelpers {
    public ConsumerHomePage loginToConsumerApp(String mobile, String otp) {
        ConsumerLoginPage consumerLoginPage= ConsumerLoginPage.getConsumerLoginPage();
        LoginInfoPage loginInfoPage = consumerLoginPage.clickOnLoginButton();
        OtpPage otpPage = loginInfoPage.enterMobileNumber(mobile).clickOnContinue();
        GPSAlert gpsAlert = otpPage.enterOtp(otp).clickOnVerifyAndProceed();
        return gpsAlert.allowGPS().allowNotifications().allowGoogleLocationService();
    }

    public void enterAndSelectLocation(ConsumerHomePage consumerHomePage, String location) {
        consumerHomePage.waitForLocationDropdownToBeVisible()
                .clickOnLocationDropdown()
                .clickOnLocationField()
                .enterLocationInLocationField(location)
                .waitForLocationSuggestionToBeDisplayed()
                .selectLocationFromSuggestion(0);
    }

    public RestaurantDetailPage openVendorTestRestaurant(RestaurantsPage restaurantsPage) {
        return restaurantsPage
                .waitForRestaurantListToBeVisible()
                .scrollToVendorRestaurant()
                .clickOnVendorRestaurant();
    }

    public CartPage addFirstItemToCart(RestaurantDetailPage restaurantDetailPage) {
        return restaurantDetailPage
                    .waitForBrowseMenuToBeDisplayed()
                    .scrollToFirstItemToAdd()
                    .clickToAddFirstItem()
                    .waitForAddItemButtonToBeDisplayed()
                    .clickOnAddItemButtonOnAddonPage()
                    .waitForViewCartToBeDisplayed()
                    .clickToViewCart();
    }

    public OrderConfirmationPage orderUsingCashOnDelivery(CheckoutPage checkoutPage) {
        return checkoutPage
                    .scrollToCashOnDelivery()
                    .clickOnCashOnDelivery()
                    .waitForPayWithCashToBeDisplayed()
                    .clickOnPayWithCash()
                    .waitAndClickOnPaymentAlertIfVisible();
    }
}
