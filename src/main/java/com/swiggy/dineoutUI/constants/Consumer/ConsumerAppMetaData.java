package com.swiggy.dineoutUI.constants.Consumer;

public interface ConsumerAppMetaData {
    String packageName = "in.swiggy.android.prod";
    String activityName = "in.swiggy.android.activities.HomeActivity";
    String appWaitPackageName = "in.swiggy.android.prod";
    String appWaitActivityName = "in.swiggy.android.activities.NewUserExperienceActivity";
}
