package com.swiggy.dineoutUI.constants.Vendor;

public interface VendorAppMetaData {
    String packageName = "in.swiggy.partnerapp";
    String activityName = "in.swiggy.partnerapp.MainActivity";
    String appWaitPackageName = "";
    String appWaitActivityName = "";
    String uatPackageName = "in.swiggy.partnerapp.uat";
    String uatActivityName = "in.swiggy.partnerapp.MainActivity";
    String uatAppWaitPackageName = "";
    String uarAppWaitActivityName = "";
}
