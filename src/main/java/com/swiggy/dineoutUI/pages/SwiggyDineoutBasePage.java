package com.swiggy.dineoutUI.pages;

import com.swiggy.automation.ui.actions.BasePage;
import com.swiggy.dineoutUI.pages.consumer.account.AccountPage;
import com.swiggy.dineoutUI.pages.consumer.home.ConsumerHomePage;
import com.swiggy.dineoutUI.pages.consumer.location.LocationPage;
import com.swiggy.dineoutUI.pages.consumer.login.ConsumerLoginPage;
import com.swiggy.dineoutUI.pages.consumer.loginInfo.LoginInfoPage;
import com.swiggy.dineoutUI.pages.consumer.otp.OtpPage;
import com.swiggy.dineoutUI.widgets.GPSAlert;
import com.swiggy.dineoutUI.widgets.LogoutConfirm;

public class SwiggyDineoutBasePage<T> extends BasePage<SwiggyDineoutBasePage> {

    public static OtpPage getOtpPage() {
        return OtpPage.getOtpPage();
    }

    public LoginInfoPage getLoginInfoPage() {
        return LoginInfoPage.getConsumerLoginInfoPage();
    }

    public GPSAlert getGPSAlert() { return new GPSAlert(); }

    public ConsumerHomePage getHomePage() { return ConsumerHomePage.getConsumerHomePage() ; }

    public AccountPage getAccountPage() { return AccountPage.getConsumerAccountPagePage(); }

    public LogoutConfirm getLogoutConfirmDialog() { return new LogoutConfirm(); }

    public ConsumerLoginPage getLoginPage() { return ConsumerLoginPage.getConsumerLoginPage(); }

    public LocationPage getConsumerLocationPage() { return LocationPage.getLocationPage(); }

}
