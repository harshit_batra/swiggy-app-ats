package com.swiggy.dineoutUI.pages.common;

import com.swiggy.automation.ui.locators.LocatorType;
import com.swiggy.dineoutUI.pages.SwiggyDineoutBasePage;
import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;

public class AppListingPageLocators extends SwiggyDineoutBasePage<AppListingPage> {
    public By PARTNER_APP = getLocator("PARTNER_APP");
    public By SWIGGY_APP = getLocator("SWIGGY_APP");

    @Override
    protected void setLocators() {
        setLocator("PARTNER_APP", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.FrameLayout[contains(@content-desc, \"Partner\")]"));
        setLocator("SWIGGY_APP", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.FrameLayout[contains(@content-desc, \"Beta\")]"));
    }
}
