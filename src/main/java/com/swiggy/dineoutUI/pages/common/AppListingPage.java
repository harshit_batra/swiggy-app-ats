package com.swiggy.dineoutUI.pages.common;

import com.swiggy.dineoutUI.pages.vendor.home.VendorHomePage;
import io.appium.java_client.android.nativekey.AndroidKey;
import org.tinylog.Logger;

public class AppListingPage extends AppListingPageLocators {
    private static AppListingPage appListingPage=null;

    private AppListingPage() {}

    public static synchronized AppListingPage getAppListingPage() {
        if(appListingPage == null) {
            appListingPage = new AppListingPage();
        }
        return appListingPage;
    }

    public void clickOnSwitchAppButton() {
        try {
            actions.keyboard().enterAndroidKey(AndroidKey.APP_SWITCH);
        }
        catch(Exception e) {
            Logger.error(e.getMessage());
        }
    }

    public void clickOnSwiggyApp() {
        actions.element().isElementDisplayed(SWIGGY_APP, 5);
        actions.element().click(SWIGGY_APP);
    }

    public void clickOnPartnerApp() {
        actions.element().isElementDisplayed(PARTNER_APP, 5);
        actions.element().click(PARTNER_APP);
    }

}
