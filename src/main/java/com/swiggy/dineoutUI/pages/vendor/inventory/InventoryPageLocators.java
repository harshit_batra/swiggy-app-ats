package com.swiggy.dineoutUI.pages.vendor.inventory;

import com.swiggy.automation.ui.locators.LocatorType;
import com.swiggy.dineoutUI.pages.SwiggyDineoutBasePage;
import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;

public class InventoryPageLocators extends SwiggyDineoutBasePage<InventoryPageLocators> {

    public By OUT_OF_STOCK_HEADING = getLocator("OUT_OF_STOCK_HEADING");

    @Override
    protected void setLocators() {
        setLocator("OUT_OF_STOCK_HEADING", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.TextView[@text=\"Out of stock items\"]"));
    }
}
