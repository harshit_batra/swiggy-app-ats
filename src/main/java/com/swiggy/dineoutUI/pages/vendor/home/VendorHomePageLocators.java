package com.swiggy.dineoutUI.pages.vendor.home;

import com.swiggy.automation.ui.locators.LocatorType;
import com.swiggy.dineoutUI.pages.SwiggyDineoutBasePage;
import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;

public class VendorHomePageLocators extends SwiggyDineoutBasePage<VendorHomePageLocators> {

    public By ORDERS_TAB = getLocator("ORDERS_TAB");
    public By INVENTORY_TAB = getLocator("INVENTORY_TAB");
    public By REPORTS_TAB = getLocator("REPORTS_TAB");
    public By MORE_TAB = getLocator("MORE_TAB");
    public By NEW_ORDERS = getLocator("NEW_ORDERS");
    public By HELP_CENTRE_TEXT = getLocator("HELP_CENTRE_TEXT");
    public By ORDER_CARD = getLocator("ORDER_CARD");
    public By PREPARING_OR_PENDING = getLocator("PREPARING_OR_PENDING");
    public By PARTNER_APP = getLocator("PARTNER_APP");
    public By SWIGGY_APP = getLocator("SWIGGY_APP");
    public static String orderId;

    @Override
    protected void setLocators() {
        setLocator("ORDERS_TAB", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.TextView[@text=\"Orders\"]"));
        setLocator("INVENTORY_TAB", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.TextView[@text=\"Inventory\"]"));
        setLocator("REPORTS_TAB", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.TextView[@text=\"Reports\"]"));
        setLocator("MORE_TAB", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.TextView[@text=\"More\"]"));
        setLocator("NEW_ORDERS", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.TextView[starts-with(@text,\"You have\") and contains(@text, \"new order\")]"));
        setLocator("HELP_CENTRE_TEXT", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.TextView[@text=\"Introducing Swiggy Help Center\"]"));
        setLocator("ORDER_CARD", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.TextView[contains(@text,\"" + orderId + "\")]"));
        setLocator("PREPARING_OR_PENDING", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.TextView[@text=\"PREPARING OR PENDING\"]"));
        setLocator("PARTNER_APP", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.FrameLayout[contains(@content-desc, \"Partner\")]"));
        setLocator("SWIGGY_APP", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.FrameLayout[contains(@content-desc, \"Beta\")]"));
    }
}
