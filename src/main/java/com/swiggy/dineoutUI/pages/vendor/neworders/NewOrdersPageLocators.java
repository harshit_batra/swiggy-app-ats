package com.swiggy.dineoutUI.pages.vendor.neworders;

import com.swiggy.automation.ui.locators.LocatorType;
import com.swiggy.dineoutUI.pages.SwiggyDineoutBasePage;
import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;

public class NewOrdersPageLocators extends SwiggyDineoutBasePage<NewOrdersPageLocators> {
    public By ORDER_CARD = getLocator("ORDER_CARD");
    public By PENDING_CONFIRMATION = getLocator("PENDING_CONFIRMATION");
    public static String orderId;

    @Override
    protected void setLocators() {
        setLocator("PENDING_CONFIRMATION", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.TextView[@text=\"Pending confirmation!\"]"));
        setLocator("ORDER_CARD", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.TextView[contains(@text,\"" + orderId + "\")]"));
    }
}
