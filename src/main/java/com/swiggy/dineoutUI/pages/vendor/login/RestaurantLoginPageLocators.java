package com.swiggy.dineoutUI.pages.vendor.login;

import com.swiggy.automation.ui.locators.LocatorType;
import com.swiggy.dineoutUI.pages.SwiggyDineoutBasePage;
import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;

public class RestaurantLoginPageLocators extends SwiggyDineoutBasePage<RestaurantLoginPageLocators> {

    public By LOGIN_FIELDS = getLocator("LOGIN_FIELDS");
    public By CONTINUE_BUTTON = getLocator("CONTINUE_BUTTON");
    public By LOGIN_BUTTON = getLocator("LOGIN_BUTTON");

@Override
protected void setLocators() {
        setLocator("LOGIN_FIELDS", LocatorType.ANDROID_LOCATOR, MobileBy.className("android.widget.EditText"));
        setLocator("CONTINUE_BUTTON", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.TextView[@text='CONTINUE']"));
        setLocator("LOGIN_BUTTON", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.TextView[@text='LOGIN']"));
    }
}
