package com.swiggy.dineoutUI.pages.vendor.orderdetail;

import com.swiggy.dineoutUI.pages.vendor.home.VendorHomePage;
import com.swiggy.dineoutUI.pages.vendor.home.VendorHomePageLocators;
import com.swiggy.dineoutUI.pages.vendor.neworders.NewOrdersPage;
import com.swiggy.mocking.pojo.Gandalf.Offers.NEW;
import io.appium.java_client.android.nativekey.AndroidKey;
import org.tinylog.Logger;

public class OrderDetailPage extends OrderDetailLocators{

    private static OrderDetailPage orderDetailsPage=null;

    private OrderDetailPage() {}

    public static synchronized OrderDetailPage getOrderDetailsPage() {
        if(orderDetailsPage == null) {
            orderDetailsPage = new OrderDetailPage();
        }
        return orderDetailsPage;
    }

    public void waitForOrderIdToBeVisible() {
        actions.element().isElementDisplayed(NEW_ORDER_ID);
    }

    public String getNewOrderIdFromOrderDetailPage() {
        return actions.element().get(NEW_ORDER_ID).getText();
    }

    public void clickOnOutOfStockNotification() {
        try {
            actions.element().isElementDisplayed(OUT_OF_STOCK_NOTIFICATION, 5);
            actions.element().click(OUT_OF_STOCK_NOTIFICATION);
        }
        catch(Exception e) {
            Logger.info("Out Of Stock Notification Not Visible");
        }
    }

    public void clickOnPrepTimeNotification() {
        try {
            actions.element().isElementDisplayed(PREP_TIME_NOTIFICATION, 5);
            actions.element().click(PREP_TIME_NOTIFICATION);
        }
        catch(Exception e) {
            Logger.info("Preparation Time Notification Not Visible");
        }
    }

    public void pressAndroidBackButton() throws Exception {
        actions.keyboard().enterAndroidKey(AndroidKey.BACK);
    }

    public void confirmOrder() {
        actions.element().click(CONFIRM_NOW);
    }

    public void markFoodReady() {
        int totalElements = actions.elements().get(FOOD_READY).size();
        Logger.info("Total Food Ready Elements " + totalElements);
        actions.elements().click(FOOD_READY, totalElements - 1);
    }

    public void goToOrdersPage() throws Exception {
        while(getVisibilityOfConfirmNow()) {
            actions.keyboard().enterAndroidKey(AndroidKey.BACK);
        }
//        actions.keyboard().enterAndroidKey(AndroidKey.APP_SWITCH);
    }

    public boolean getVisibilityOfConfirmNow() {
        return actions.element().isElementDisplayed(CONFIRM_NOW, 3);
    }
}
