package com.swiggy.dineoutUI.pages.vendor.neworders;

import com.swiggy.automation.ui.locators.LocatorType;
import com.swiggy.dineoutUI.pages.consumer.home.ConsumerHomePage;
import com.swiggy.dineoutUI.pages.vendor.orderdetail.OrderDetailPage;
import io.appium.java_client.MobileBy;

public class NewOrdersPage extends NewOrdersPageLocators {

    private static NewOrdersPage newOrdersPage=null;

    private NewOrdersPage() {}

    public static synchronized NewOrdersPage getNewOrdersPage() {
        if(newOrdersPage == null) {
            newOrdersPage = new NewOrdersPage();
        }
        return newOrdersPage;
    }

    public void setLocatorForOrderCard(String orderId) {
        setLocator("ORDER_CARD", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.TextView[contains(@text,\"" + orderId + "\")]"));
    }

    public OrderDetailPage scrollAndClickOnOrderCard() {
        actions.scroll().swipeUp(ORDER_CARD);
        actions.element().click(ORDER_CARD);
        return OrderDetailPage.getOrderDetailsPage();
    }

    public void waitForPendingConfirmationTextToBeVisible() {
        actions.element().isElementDisplayed(PENDING_CONFIRMATION);
    }
}
