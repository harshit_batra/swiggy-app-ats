package com.swiggy.dineoutUI.pages.vendor.inventory;

import com.swiggy.dineoutUI.pages.consumer.restaurants.RestaurantsPage;

public class InventoryPage extends InventoryPageLocators{

    private static InventoryPage inventoryPage=null;

    private InventoryPage() {}

    public static synchronized InventoryPage getInventoryPage() {
        if(inventoryPage == null) {
            inventoryPage = new InventoryPage();
        }
        return inventoryPage;
    }

    public void waitForOutOfStockHeadingToBeDisplayed() {
        actions.element().isElementDisplayed(OUT_OF_STOCK_HEADING, 5);
    }
}
