package com.swiggy.dineoutUI.pages.vendor.home;

import com.swiggy.automation.ui.listeners.DriverCreationListener;
import com.swiggy.dineoutUI.pages.consumer.OrderConfirmation.OrderConfirmationPage;
import com.swiggy.dineoutUI.pages.vendor.inventory.InventoryPage;
import com.swiggy.dineoutUI.pages.vendor.neworders.NewOrdersPage;
import com.swiggy.dineoutUI.pages.vendor.orderdetail.OrderDetailPage;
import io.appium.java_client.TouchAction;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.nativekey.AndroidKey;
import io.appium.java_client.touch.WaitOptions;
import io.appium.java_client.touch.offset.PointOption;
import org.openqa.selenium.Dimension;
import org.tinylog.Logger;

import java.time.Duration;

public class VendorHomePage extends VendorHomePageLocators {

    private static VendorHomePage vendorHomePage=null;

    private VendorHomePage() {}

    public static synchronized VendorHomePage getVendorHomePage() {
        if(vendorHomePage == null) {
            vendorHomePage = new VendorHomePage();
        }
        return vendorHomePage;
    }

    public boolean getVisibilityOfPreparingState() {
        return actions.element().isElementDisplayed(PREPARING_OR_PENDING, 3);
    }

    public VendorHomePage waitForMoreTabToBeVisible() {
        actions.element().isElementDisplayed(MORE_TAB, 5);
        return this;
    }

    public void waitAndCloseHelpCentreText() {
        try {
            actions.element().isElementDisplayed(HELP_CENTRE_TEXT, 5);
            actions.element().click(HELP_CENTRE_TEXT);
        }
        catch (Exception e) {
            Logger.info("Help Centre Text Message Not Visible");
        }
    }

    public InventoryPage clickOnInventoryTab() {
        actions.element().click(INVENTORY_TAB);
        return InventoryPage.getInventoryPage();
    }

    public VendorHomePage clickOnOrdersTab() {
        actions.element().click(ORDERS_TAB);
        return this;
    }

    public VendorHomePage clickOnMoreTab() {
        actions.element().click(MORE_TAB);
        return this;
    }

    public int getCurrentNewOrders() {
        int newOrders = 0;
        try {
            actions.element().isElementDisplayed(NEW_ORDERS, 5);
            String text = actions.element().getText(NEW_ORDERS);
            if(text.contains(" a ")) {
                newOrders = 1;
            } else {
                newOrders = Integer.parseInt(text.split(" ")[2]);
            }
        }
        catch (Exception e) {
            Logger.info("There are no new orders");
        }
        Logger.info("Value of newOrders is " + newOrders);
        return newOrders;
    }

    public OrderDetailPage clickOnNewOrders() {
        actions.element().click(NEW_ORDERS);
        return OrderDetailPage.getOrderDetailsPage();
    }

    public OrderDetailPage scrollAndClickOnPreparingOrder() {
        actions.scroll().swipeUp(ORDER_CARD);
        actions.element().click(ORDER_CARD);
        return OrderDetailPage.getOrderDetailsPage();
    }

    public void refreshTheScreenBySwiping() {
        final int ANIMATION_TIME = 5000; // ms

        final int PRESS_TIME = 1000; // ms

        int edgeBorder = 10; // better avoid edges
        PointOption pointOptionStart, pointOptionEnd;

        // init screen variables
        AndroidDriver driver = (AndroidDriver) DriverCreationListener.driver.get();
        Dimension dims = driver.manage().window().getSize();

        // init start point = center of screen
        pointOptionStart = PointOption.point(dims.width / 2, dims.height / 2);

//        switch (dir) {
//            case DOWN: // center of footer
                pointOptionEnd = PointOption.point(dims.width / 2, dims.height - edgeBorder);
//                break;
//            case UP: // center of header
//                pointOptionEnd = PointOption.point(dims.width / 2, edgeBorder);
//                break;
//            case LEFT: // center of left side
//                pointOptionEnd = PointOption.point(edgeBorder, dims.height / 2);
//                break;
//            case RIGHT: // center of right side
//                pointOptionEnd = PointOption.point(dims.width - edgeBorder, dims.height / 2);
//                break;
//            default:
//                throw new IllegalArgumentException("swipeScreen(): dir: '" + dir + "' NOT supported");
//        }

        // execute swipe using TouchAction
        try {
            new TouchAction<>(driver)
                    .press(pointOptionStart)
                    // a bit more reliable when we add small wait
                    .waitAction(WaitOptions.waitOptions(Duration.ofMillis(PRESS_TIME)))
                    .moveTo(pointOptionEnd)
                    .release().perform();
        } catch (Exception e) {
            System.err.println("swipeScreen(): TouchAction FAILED\n" + e.getMessage());
            return;
        }

        // always allow swipe action to complete
        try {
            Thread.sleep(ANIMATION_TIME);
        } catch (InterruptedException e) {
            // ignore
        }
    }

    public void removeMinimizedConsumerAppFromScreen() {
        AndroidDriver driver = (AndroidDriver) DriverCreationListener.driver.get();
        int edgeBorder = 200; // better avoid edges
        PointOption pointOptionStart, pointOptionEnd;

        // init screen variables
        Dimension dims = driver.manage().window().getSize();

        // init start point = Bottom Right Corner
        pointOptionStart = PointOption.point(dims.width - edgeBorder, dims.height - edgeBorder);

        // end point = Around middle and completely at bottom
        pointOptionEnd = PointOption.point(dims.width - 500, dims.height - 1);
        try {
            new TouchAction<>(driver)
                    .press(pointOptionStart)
//                    .waitAction(WaitOptions.waitOptions(Duration.ofMillis(PRESS_TIME)))
                    .moveTo(pointOptionEnd)
                    .release().perform();
        } catch (Exception e) {
            System.err.println("swipeScreen(): TouchAction FAILED\n" + e.getMessage());
            return;
        }

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            // ignore
        }
    }
}
