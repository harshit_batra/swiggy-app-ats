package com.swiggy.dineoutUI.pages.vendor.login;

import com.swiggy.dineoutUI.pages.vendor.home.VendorHomePage;
import com.swiggy.dineoutUI.pages.vendor.inventory.InventoryPage;
import org.tinylog.Logger;

public class RestaurantLoginPage extends RestaurantLoginPageLocators {

    private static RestaurantLoginPage restaurantLoginPage=null;

    private RestaurantLoginPage() {}

    public static synchronized RestaurantLoginPage getRestaurantLoginPage() {
        if(restaurantLoginPage == null) {
            restaurantLoginPage = new RestaurantLoginPage();
        }
        return restaurantLoginPage;
    }

//    public RestaurantLoginPage waitForMobileFieldToBeVisible(String mobileOrId) {
//        actions.element().isElementDisplayed(LOGIN_FIELDS, 10);
//        actions.element().textbox().type(LOGIN_FIELDS, mobileOrId);
//        return this;
//    }

    public RestaurantLoginPage enterMobileNumberOrId(String mobileOrId) {
        actions.element().isElementDisplayed(LOGIN_FIELDS, 10);
        actions.element().textbox().type(LOGIN_FIELDS, mobileOrId);
        return this;
    }

    public RestaurantLoginPage enterPasswordOrOtp(String passwordOrOtp) {
        actions.element().isElementDisplayed(LOGIN_BUTTON, 10);
        actions.elements().get(LOGIN_FIELDS, 1).sendKeys(passwordOrOtp);
        return this;
    }

    public RestaurantLoginPage clickOnContinueButton() {
        Logger.info("Text in mobile or id Field is before" + actions.elements().get(LOGIN_FIELDS, 0).getText() + "after");
        actions.element().click(CONTINUE_BUTTON);
        return this;
    }

    public VendorHomePage clickOnLoginButton() {
        actions.element().click(LOGIN_BUTTON);
        return VendorHomePage.getVendorHomePage();
    }

    public RestaurantLoginPage waitForLoginButtonToBeVisible() {
        actions.element().isElementDisplayed(LOGIN_BUTTON, 10);
        return this;
    }
}
