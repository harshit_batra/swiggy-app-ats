package com.swiggy.dineoutUI.pages.vendor.orderdetail;

import com.swiggy.automation.ui.locators.LocatorType;
import com.swiggy.dineoutUI.pages.SwiggyDineoutBasePage;
import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;

public class OrderDetailLocators extends SwiggyDineoutBasePage<OrderDetailLocators> {
    public By NEW_ORDER_ID = getLocator("NEW_ORDER_ID");
    public By EDITED_ORDER_ID = getLocator("EDITED_ORDER_ID");
    public By OUT_OF_STOCK_NOTIFICATION = getLocator("OUT_OF_STOCK_NOTIFICATION");
    public By CONFIRM_NOW = getLocator("CONFIRM_NOW");
    public By FOOD_READY = getLocator("FOOD_READY");
    public By PREP_TIME_NOTIFICATION = getLocator("PREP_TIME_NOTIFICATION");

    @Override
    protected void setLocators() {
        setLocator("NEW_ORDER_ID", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.TextView[starts-with(@text,\"#\") and contains(@text, \"NEW\")]"));
        setLocator("EDITED_ORDER_ID", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.TextView[starts-with(@text,\"#\") and contains(@text, \"EDITED\")]"));
        setLocator("OUT_OF_STOCK_NOTIFICATION", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.TextView[starts-with(@text,\"Item(s)\") and contains(@text, \"out of stock\")]"));
        setLocator("CONFIRM_NOW", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.TextView[@text=\"CONFIRM NOW\"]"));
        setLocator("FOOD_READY", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.TextView[@text=\"FOOD READY\"]"));
        setLocator("PREP_TIME_NOTIFICATION", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.TextView[starts-with(@text,\"Confirm\") and contains(@text, \"kitchen will take\")]"));
    }
}
