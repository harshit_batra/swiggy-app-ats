package com.swiggy.dineoutUI.pages.consumer.otp;

import com.swiggy.dineoutUI.pages.consumer.loginInfo.LoginInfoPage;
import com.swiggy.dineoutUI.widgets.GPSAlert;

public class OtpPage extends OtpPageLocators {

    private static OtpPage otpPage=null;

    private OtpPage() {}

    public static synchronized OtpPage getOtpPage() {
        if(otpPage == null) {
            otpPage = new OtpPage();
        }
        return otpPage;
    }

    public OtpPage enterOtp(String otp) {
        actions.element().isElementDisplayed(OTP_TEXT_BOX, 5);
        actions.element().textbox().type(OTP_TEXT_BOX, otp);
        return this;
    }

    public GPSAlert clickOnVerifyAndProceed() {
        actions.element().click(VERIFY_AND_PROCEED);
        return new GPSAlert();
    }
}
