package com.swiggy.dineoutUI.pages.consumer.location;

import com.swiggy.automation.ui.locators.LocatorType;
import com.swiggy.dineoutUI.pages.SwiggyDineoutBasePage;
import com.swiggy.dineoutUI.pages.consumer.home.ConsumerHomePageLocators;
import org.openqa.selenium.By;

public class LocationPageLocators extends SwiggyDineoutBasePage<ConsumerHomePageLocators> {

    public By LOCATION_TEXT_FIELD = getLocator("LOCATION_TEXT_FIELD");
    public By USER_SAVED_LOCATION = getLocator("USER_SAVED_LOCATION");

    @Override
    protected void setLocators() {
        setLocator("LOCATION_TEXT_FIELD", LocatorType.ANDROID_LOCATOR, By.id("location_description"));
        setLocator("USER_SAVED_LOCATION", LocatorType.ANDROID_LOCATOR, By.id("saved_location_title_text"));
    }
}
