package com.swiggy.dineoutUI.pages.consumer.location;

import com.swiggy.dineoutUI.pages.consumer.explore.ExplorePage;

public class LocationPage extends LocationPageLocators {

    private static LocationPage locationPage=null;

    private LocationPage() {}

    public static synchronized LocationPage getLocationPage() {
        if(locationPage == null) {
            locationPage = new LocationPage();
        }
        return locationPage;
    }

    public LocationPage clickOnLocationField() {
        actions.element().click(LOCATION_TEXT_FIELD);
        return this;
    }

    public LocationPage enterLocationInLocationField(String location) {
        actions.element().textbox().type(LOCATION_TEXT_FIELD, location);
        return this;
    }

    public LocationPage waitForLocationSuggestionToBeDisplayed() {
        actions.element().isElementDisplayed(USER_SAVED_LOCATION, 5);
        return this;
    }

    public void selectLocationFromSuggestion(int index) {
        actions.elements().click(USER_SAVED_LOCATION, 0);
    }
}
