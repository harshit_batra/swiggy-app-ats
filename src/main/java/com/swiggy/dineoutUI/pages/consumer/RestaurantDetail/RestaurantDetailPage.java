package com.swiggy.dineoutUI.pages.consumer.RestaurantDetail;

import com.swiggy.dineoutUI.pages.consumer.Cart.CartPage;
import com.swiggy.dineoutUI.pages.consumer.otp.OtpPage;
import org.tinylog.Logger;

public class RestaurantDetailPage extends RestaurantDetailLocators {

    private static RestaurantDetailPage restaurantDetailPage=null;

    private RestaurantDetailPage() {}

    public static synchronized RestaurantDetailPage getRestaurantDetailPage() {
        if(restaurantDetailPage == null) {
            restaurantDetailPage = new RestaurantDetailPage();
        }
        return restaurantDetailPage;
    }

    public RestaurantDetailPage waitForBrowseMenuToBeDisplayed() {
        actions.element().isElementDisplayed(MENU, 5);
        return this;
    }

    public RestaurantDetailPage clickOnBrowseMenu() {
        actions.element().click(MENU);
        return this;
    }

    public RestaurantDetailPage waitForMenuCategoriesToBeDisplayed() {
        actions.element().isElementDisplayed(MENU_CATEGORY);
        return this;
    }

    public RestaurantDetailPage clickOnMenuCategory(int index) {
        actions.elements().click(MENU_CATEGORY, 1);
        return this;
    }

    public RestaurantDetailPage waitForSearchIconToBeDisplayed() {
        actions.element().isElementDisplayed(SEARCH_ICON);
        return this;
    }

    public RestaurantDetailPage clickOnSearchIcon() {
        actions.element().click(SEARCH_ICON);
        return this;
    }

    public RestaurantDetailPage waitForSearchAreaToBeDisplayed() {
        actions.element().isElementDisplayed(SEARCH_TEXTAREA);
        return this;
    }

    public RestaurantDetailPage enterDishName(String dish) {
        actions.element().textbox().type(SEARCH_TEXTAREA, dish);
        return this;
    }

    public RestaurantDetailPage scrollToFirstItemToAdd(){
        actions.scroll().swipeUp(FIRST_ITEM);
        return this;
    }

    public RestaurantDetailPage clickToAddFirstItem() {
        actions.element().click(FIRST_ITEM);
        return this;
    }

    public RestaurantDetailPage waitForAddItemButtonToBeDisplayed() {
        try {
            actions.element().isElementDisplayed(ADD_ITEM_BUTTON, 5);
        }
        catch (Exception e) {
            Logger.info("Add item button not displayed, item is directly added to cart");
        }
        return this;
    }

    public RestaurantDetailPage clickOnAddItemButtonOnAddonPage() {
        try {
            actions.element().click(ADD_ITEM_BUTTON);
        }
        catch (Exception e) {
            Logger.info("Add item button not displayed, item is directly added to cart");
        }
        return this;
    }

    public RestaurantDetailPage waitForViewCartToBeDisplayed() {
        actions.element().isElementDisplayed(VIEW_CART, 5);
        return this;
    }

    public CartPage clickToViewCart() {
        actions.element().click(VIEW_CART);
        return CartPage.getCartPage();
    }
}
