package com.swiggy.dineoutUI.pages.consumer.explore;

import com.swiggy.dineoutUI.pages.consumer.checkout.CheckoutPage;

public class ExplorePage extends ExplorePageLocators {

    private static ExplorePage explorePage=null;

    private ExplorePage() {}

    public static synchronized ExplorePage getExplorePage() {
        if(explorePage == null) {
            explorePage = new ExplorePage();
        }
        return explorePage;
    }

    public ExplorePage searchForRestaurant(String restaurantToSearch) {
        actions.element().click(SEARCH_TEXT_FIELD);
        actions.element().textbox().type(SEARCH_TEXT_FIELD, restaurantToSearch);
        actions.keyboard().enter();
      //  actions.keyboard().hide();
//        if (CommonHelpers.getCurrentPlatform().equals(Platform.ANDROID)) {
//            //TODO :: To check the getMethod without index
//            actions.elements().get(SEARCH_SUGGESTION_UNDER_CATEGORY, "Restaurant", true).click();
//            //   actions.elements().click(SEARCH_AUTO_SUGGESTION_LIST_NAME, restaurantToSearch,true);
 //       }
        return this;
    }
}
