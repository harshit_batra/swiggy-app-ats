package com.swiggy.dineoutUI.pages.consumer.otp;

import com.swiggy.automation.ui.locators.LocatorType;
import com.swiggy.dineoutUI.pages.SwiggyDineoutBasePage;
import org.openqa.selenium.By;

public class OtpPageLocators extends SwiggyDineoutBasePage<OtpPageLocators> {

    public By OTP_TEXT_BOX = getLocator("OTP_TEXT_BOX");
    public By VERIFY_AND_PROCEED = getLocator("VERIFY_AND_PROCEED");

    @Override
    protected void setLocators() {
        setLocator("OTP_TEXT_BOX",LocatorType.ANDROID_LOCATOR, By.id("otpField"));

        setLocator("VERIFY_AND_PROCEED", LocatorType.ANDROID_LOCATOR, By.id("forgotPasswordSubmitBtn"));
    }
}
