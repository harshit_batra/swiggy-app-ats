package com.swiggy.dineoutUI.pages.consumer.loginInfo;

import com.swiggy.dineoutUI.pages.consumer.login.ConsumerLoginPage;
import com.swiggy.dineoutUI.pages.consumer.otp.OtpPage;

public class LoginInfoPage extends LoginInfoLocators {

    private static LoginInfoPage consumerLoginInfoPage=null;

    private LoginInfoPage() {}

    public static synchronized LoginInfoPage getConsumerLoginInfoPage() {
        if(consumerLoginInfoPage == null) {
            consumerLoginInfoPage = new LoginInfoPage();
        }
        return consumerLoginInfoPage;
    }

    public LoginInfoPage enterMobileNumber(String mobileNumber) {
        actions.element().isElementDisplayed(MOBILE_NUMBER, 5);
        actions.element().textbox().type(MOBILE_NUMBER, mobileNumber);
        return this;
    }

    public OtpPage clickOnContinue() {
        actions.element().click(CONTINUE_BUTTON);
        return OtpPage.getOtpPage();
    }
}
