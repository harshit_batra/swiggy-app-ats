package com.swiggy.dineoutUI.pages.consumer.Cart;

import com.swiggy.automation.ui.locators.LocatorType;
import com.swiggy.dineoutUI.pages.SwiggyDineoutBasePage;
import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;

public class CartPageLocators extends SwiggyDineoutBasePage<CartPageLocators> {
    public By PROCEED_TO_PAY = getLocator("PROCEED_TO_PAY");
    public By CART_ITEMS = getLocator("CART_ITEMS");

    @Override
    protected void setLocators() {
        setLocator("PROCEED_TO_PAY", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.TextView[@text=\"PROCEED TO PAY\"]"));
        setLocator("CART_ITEMS", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.TextView[contains(@content-desc, \", quantity\")]"));
    }
}
