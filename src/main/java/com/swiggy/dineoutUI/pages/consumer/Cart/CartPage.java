package com.swiggy.dineoutUI.pages.consumer.Cart;

import com.swiggy.dineoutUI.pages.consumer.account.AccountPage;
import com.swiggy.dineoutUI.pages.consumer.checkout.CheckoutPage;

public class CartPage extends CartPageLocators{

    private static CartPage cartPage=null;

    private CartPage() {}

    public static synchronized CartPage getCartPage() {
        if(cartPage == null) {
            cartPage = new CartPage();
        }
        return cartPage;
    }

    public CheckoutPage waitAndClickOnProceedToPayButton() {
        actions.element().isElementDisplayed(PROCEED_TO_PAY);
        actions.element().click(PROCEED_TO_PAY);
        return CheckoutPage.getCheckoutPage();
    }

    public String getCartItemName(int index) {
        return actions.elements().get(CART_ITEMS, index).getText();
    }

    public String getCartItemQuantity(int index) {
        String quantityText =  actions.elements().get(CART_ITEMS, index).getAttribute("content-desc");
        return quantityText.substring(quantityText.length() - 1);
    }
}
