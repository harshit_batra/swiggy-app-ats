package com.swiggy.dineoutUI.pages.consumer.login;

import com.swiggy.automation.ui.locators.LocatorType;
import com.swiggy.dineoutUI.pages.SwiggyDineoutBasePage;
import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;

public class ConsumerLoginPageLocators extends SwiggyDineoutBasePage<ConsumerLoginPageLocators> {

    public By LOGIN_BUTTON = getLocator("LOGIN");

    @Override
    protected void setLocators() {
        setLocator("LOGIN", LocatorType.ANDROID_LOCATOR, MobileBy.id("item_menu_top_header_restaurant_name3"));
    }
}
