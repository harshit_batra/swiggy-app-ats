package com.swiggy.dineoutUI.pages.consumer.restaurants;

import com.swiggy.dineoutUI.pages.consumer.RestaurantDetail.RestaurantDetailPage;

public class RestaurantsPage extends RestaurantLocators{

    private static RestaurantsPage restaurantPage=null;

    private RestaurantsPage() {}

    public static synchronized RestaurantsPage getRestaurantPage() {
        if(restaurantPage == null) {
            restaurantPage = new RestaurantsPage();
        }
        return restaurantPage;
    }

    public RestaurantsPage waitForRestaurantListToBeVisible() {
        actions.element().isElementDisplayed(RESTAURANT_LIST, 5);
        return this;
    }

    public RestaurantsPage scrollToVendorRestaurant() {
        actions.scroll().swipeUp(VENDOR_RESTAURANT);
        return this;
    }

    public RestaurantDetailPage clickOnVendorRestaurant() {
        actions.element().click(VENDOR_RESTAURANT);
        return RestaurantDetailPage.getRestaurantDetailPage();
    }
}
