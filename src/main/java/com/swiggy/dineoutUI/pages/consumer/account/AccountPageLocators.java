package com.swiggy.dineoutUI.pages.consumer.account;

import com.swiggy.automation.ui.locators.LocatorType;
import com.swiggy.dineoutUI.pages.SwiggyDineoutBasePage;
import org.openqa.selenium.By;

public class AccountPageLocators extends SwiggyDineoutBasePage<AccountPageLocators> {

    public By LOGOUT = getLocator("LOGOUT");

    @Override
    protected void setLocators() {
    //    setLocator("LOGOUT", LocatorType.ANDROID_LOCATOR, By.name("LOGOUT"));
        setLocator("LOGOUT", LocatorType.ANDROID_LOCATOR, By.xpath("(//android.view.ViewGroup[contains(@content-desc,'Logout')])"));

    }
}
