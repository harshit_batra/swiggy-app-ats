package com.swiggy.dineoutUI.pages.consumer.OrderConfirmation;

import com.swiggy.automation.ui.locators.LocatorType;
import com.swiggy.dineoutUI.pages.SwiggyDineoutBasePage;
import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;

public class OrderConfirmationPageLocators extends SwiggyDineoutBasePage<OrderConfirmationPageLocators> {
    public By OUTLET_DETAILS = getLocator("OUTLET_DETAILS");
    public By DELIVERY_DETAILS = getLocator("DELIVERY_DETAILS");
    public By ORDER_NUMBER = getLocator("ORDER_NUMBER");
    public By PREPARING_FOOD_STATUS = getLocator("PREPARING_FOOD_STATUS");
    public By OUTLET_MESSAGE = getLocator("OUTLET_MESSAGE");
    public By HOME_BACK_BUTTON = getLocator("HOME_BACK_BUTTON");

    @Override
    protected void setLocators() {
        setLocator("OUTLET_DETAILS", LocatorType.ANDROID_LOCATOR, MobileBy.id("outlet_details_view"));
        setLocator("DELIVERY_DETAILS", LocatorType.ANDROID_LOCATOR, MobileBy.id("delivery_details_view"));
        setLocator("ORDER_NUMBER", LocatorType.ANDROID_LOCATOR, MobileBy.id("dish_detail_title"));
        setLocator("OUTLET_MESSAGE", LocatorType.ANDROID_LOCATOR, MobileBy.id("outlet_message_textview"));
        setLocator("HOME_BACK_BUTTON", LocatorType.ANDROID_LOCATOR, MobileBy.id("home_back_button"));
        setLocator("PREPARING_FOOD_STATUS", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.TextView[contains(@text, \"preparing\")]"));
    }
}
