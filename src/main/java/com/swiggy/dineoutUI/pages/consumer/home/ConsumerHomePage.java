package com.swiggy.dineoutUI.pages.consumer.home;

import com.swiggy.dineoutUI.pages.consumer.explore.ExplorePage;
import com.swiggy.dineoutUI.pages.consumer.location.LocationPage;
import com.swiggy.dineoutUI.pages.consumer.restaurants.RestaurantsPage;
import org.tinylog.Logger;

public class ConsumerHomePage extends ConsumerHomePageLocators {

    private static ConsumerHomePage consumerHomePage=null;

    private ConsumerHomePage() {}

    public static synchronized ConsumerHomePage getConsumerHomePage() {
        if(consumerHomePage == null) {
            consumerHomePage = new ConsumerHomePage();
        }
        return consumerHomePage;
    }

    public ConsumerHomePage waitForAccountOptionToBeVisible() {
        actions.element().isElementDisplayed(ACCOUNT_ICON, 10);
        return this;
    }

    public ConsumerHomePage waitForLocationDropdownToBeVisible() {
        actions.element().isElementDisplayed(LOCATION_DROPDOWN_ICON, 5);
        return this;
    }

    public ConsumerHomePage clickOnAccountIcon() {
        actions.element().click(ACCOUNT_ICON);
        return this;
    }

    public LocationPage clickOnLocationDropdown() {
        actions.element().click(LOCATION_DROPDOWN_ICON);
        return LocationPage.getLocationPage();
    }

    public ConsumerHomePage waitForExploreIconToBeVisible() {
        actions.element().isElementDisplayed(SEARCH_ICON, 10);
        return this;
    }

    public ExplorePage clickOnExploreIcon() {
        actions.element().click(SEARCH_ICON);
        return ExplorePage.getExplorePage();
    }

    public ConsumerHomePage waitForVideoPopUpToBeVisible() {
        try {
            actions.element().isElementDisplayed(VIDEO_POP_UP_CLOSE_BTN, 5);
        } catch (Exception e) {
            Logger.info("Video not shown");
        }
        return this;
    }

    public void clickOnVideoPopupCloseButton() {
        try {
            actions.element().click(VIDEO_POP_UP_CLOSE_BTN);
        } catch (Exception e) {
            Logger.info("Cannot click on Popup Close Button, may be because its not visible");
        }
    }

    public void selectRestaurantFromHomePage(int index) {
        actions.elements().click(RESTAURANT_LIST, 0);
    }

    public RestaurantsPage waitAndClickOnAllRestaurants() {
        actions.element().isElementDisplayed(ALL_RESTAURANTS, 5);
        actions.element().click(ALL_RESTAURANTS);
        return RestaurantsPage.getRestaurantPage();
    }

    public ConsumerHomePage waitForAllRestaurantsToBeVisible() {
        actions.element().isElementDisplayed(ALL_RESTAURANTS, 10);
        return this;
    }
}
