package com.swiggy.dineoutUI.pages.consumer.checkout;

import com.swiggy.dineoutUI.pages.consumer.Cart.CartPage;
import com.swiggy.dineoutUI.pages.consumer.OrderConfirmation.OrderConfirmationPage;
import org.tinylog.Logger;

public class CheckoutPage extends CheckoutPageLocators {

    private static CheckoutPage checkoutPage=null;

    private CheckoutPage() {}

    public static synchronized CheckoutPage getCheckoutPage() {
        if(checkoutPage == null) {
            checkoutPage = new CheckoutPage();
        }
        return checkoutPage;
    }

    public CheckoutPage scrollToCashOnDelivery() {
        actions.scroll().swipeUp(CASH_ON_DELIVERY);
        return this;
    }

    public CheckoutPage clickOnCashOnDelivery() {
        actions.element().click(CASH_ON_DELIVERY);
        return this;
    }

    public CheckoutPage waitForPayWithCashToBeDisplayed() {
        actions.element().isElementDisplayed(PAY_WITH_CASH, 5);
        return this;
    }

    public CheckoutPage clickOnPayWithCash() {
        actions.element().click(PAY_WITH_CASH);
        return this;
    }

    public CheckoutPage clickOnUnderstandCancelPolicy() {
        try {
            actions.element().click(CANCEL_POLICY);
        }
        catch (Exception e) {
            Logger.info("Cancellation policy Window not clickable");
        }
        return this;
    }

    public OrderConfirmationPage waitAndClickOnPaymentAlertIfVisible() {
        try {
            actions.element().isElementDisplayed(PAYMENT_ALERT_CONTENT, 5);
            actions.element().click(PAYMENT_ALERT_BUTTON);
        } catch (Exception e) {
            Logger.info("Payment Alert Not Visible");
        }
        return OrderConfirmationPage.getOrderConfirmationPagePage();
    }
}
