package com.swiggy.dineoutUI.pages.consumer.explore;

import com.swiggy.automation.ui.locators.LocatorType;
import com.swiggy.dineoutUI.pages.SwiggyDineoutBasePage;
import org.openqa.selenium.By;

public class ExplorePageLocators extends SwiggyDineoutBasePage<ExplorePageLocators> {

    public By SEARCH_TEXT_FIELD = getLocator("SEARCH_TEXT_FIELD");

    @Override
    protected void setLocators() {
        setLocator("SEARCH_TEXT_FIELD", LocatorType.ANDROID_LOCATOR, By.id("search_query"));
    }
}
