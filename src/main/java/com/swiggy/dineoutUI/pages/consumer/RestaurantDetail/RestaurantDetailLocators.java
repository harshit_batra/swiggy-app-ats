package com.swiggy.dineoutUI.pages.consumer.RestaurantDetail;

import com.swiggy.automation.ui.locators.LocatorType;
import com.swiggy.dineoutUI.pages.SwiggyDineoutBasePage;
import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;

public class RestaurantDetailLocators extends SwiggyDineoutBasePage<RestaurantDetailLocators> {
    public By MENU = getLocator("MENU");
    public By MENU_CATEGORY = getLocator("MENU_CATEGORY");
    public By SEARCH_ICON = getLocator("SEARCH_ICON");
    public By SEARCH_TEXTAREA = getLocator("SEARCH_TEXTAREA");
    public By FIRST_ITEM = getLocator("FIRST_ITEM");
    public By ADD_ITEM_BUTTON = getLocator("ADD_ITEM_BUTTON");
    public By VIEW_CART = getLocator("VIEW_CART");

    @Override
    protected void setLocators() {
        setLocator("MENU", LocatorType.ANDROID_LOCATOR, MobileBy.id("menu_fab"));
        setLocator("MENU_CATEGORY", LocatorType.ANDROID_LOCATOR, MobileBy.id("menu_ctegory_fab_item"));
        setLocator("SEARCH_ICON", LocatorType.ANDROID_LOCATOR, MobileBy.id("search_icon"));
        setLocator("SEARCH_TEXTAREA", LocatorType.ANDROID_LOCATOR, MobileBy.id("menu_search_edit_text"));
        setLocator("FIRST_ITEM", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("(//android.widget.RelativeLayout[@content-desc=\"Add Item\"])[1]"));
        setLocator("ADD_ITEM_BUTTON", LocatorType.ANDROID_LOCATOR, MobileBy.id("add_item"));
        setLocator("VIEW_CART", LocatorType.ANDROID_LOCATOR, MobileBy.id("tv_checkout"));
    }
}
