package com.swiggy.dineoutUI.pages.consumer.loginInfo;

import com.swiggy.automation.ui.locators.LocatorType;
import com.swiggy.dineoutUI.pages.SwiggyDineoutBasePage;
import org.openqa.selenium.By;

public class LoginInfoLocators extends SwiggyDineoutBasePage<LoginInfoLocators> {

    public By MOBILE_NUMBER = getLocator("MOBILE_NUMBER");
    public By CONTINUE_BUTTON=getLocator("CONTINUE_BUTTON");

    @Override
    protected void setLocators() {
        setLocator("MOBILE_NUMBER", LocatorType.ANDROID_LOCATOR, By.id("loginCheckPhoneNumberEditText"));

        setLocator("CONTINUE_BUTTON", LocatorType.ANDROID_LOCATOR, By.id("loginCheckButton"));
    }
}
