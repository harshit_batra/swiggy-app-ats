package com.swiggy.dineoutUI.pages.consumer.OrderConfirmation;

import com.swiggy.dineoutUI.pages.consumer.home.ConsumerHomePage;

public class OrderConfirmationPage extends OrderConfirmationPageLocators {

    private static OrderConfirmationPage orderConfirmationPage=null;

    private OrderConfirmationPage() {}

    public static synchronized OrderConfirmationPage getOrderConfirmationPagePage() {
        if(orderConfirmationPage == null) {
            orderConfirmationPage = new OrderConfirmationPage();
        }
        return orderConfirmationPage;
    }

    public OrderConfirmationPage waitForOutletDetailsToBeDisplayed() {
        actions.element().isElementDisplayed(OUTLET_DETAILS, 15);
        return this;
    }

    public void waitForDeliveryDetailsToBeDisplayed() {
        actions.element().isElementDisplayed(DELIVERY_DETAILS, 10);
    }

    public String getConsumerOrderId() {
        String orderNumberText = actions.element().get(ORDER_NUMBER).getText();
        return orderNumberText.substring(orderNumberText.indexOf('#') + 1);
    }

    public String getVendorOrderId() {
        String orderNumberText = actions.element().get(ORDER_NUMBER).getText();
        return orderNumberText.substring(orderNumberText.length() - 4);
    }

    public String getOutletMessage() {
        return actions.element().get(OUTLET_MESSAGE).getText();
    }

    public void clickOnHomeBackButton() {
        actions.element().click(HOME_BACK_BUTTON);
    }
}
