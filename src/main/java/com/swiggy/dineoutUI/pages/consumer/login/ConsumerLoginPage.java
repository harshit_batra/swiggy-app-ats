package com.swiggy.dineoutUI.pages.consumer.login;

import com.swiggy.dineoutUI.pages.consumer.location.LocationPage;
import com.swiggy.dineoutUI.pages.consumer.loginInfo.LoginInfoPage;

public class ConsumerLoginPage extends ConsumerLoginPageLocators {

    private static ConsumerLoginPage consumerLoginPage=null;

    private ConsumerLoginPage() {}

    public static synchronized ConsumerLoginPage getConsumerLoginPage() {
        if(consumerLoginPage == null) {
            consumerLoginPage = new ConsumerLoginPage();
        }
        return consumerLoginPage;
    }

    public LoginInfoPage clickOnLoginButton() {
        actions.element().isElementDisplayed(LOGIN_BUTTON, 5);
        actions.element().click(LOGIN_BUTTON);
        return LoginInfoPage.getConsumerLoginInfoPage();
    }

    public ConsumerLoginPage checkIfLoginButtonIsVisibleOrNot() {
        actions.element().isElementDisplayed(LOGIN_BUTTON, 5);
        return this;
    }
}
