package com.swiggy.dineoutUI.pages.consumer.account;

import com.swiggy.dineoutUI.pages.common.AppListingPage;

public class AccountPage extends AccountPageLocators{

    private static AccountPage accountPage=null;

    private AccountPage() {}

    public static synchronized AccountPage getConsumerAccountPagePage() {
        if(accountPage == null) {
            accountPage = new AccountPage();
        }
        return accountPage;
    }

    public AccountPage scrollToLogoutButton() {
        actions.scroll().swipeUp(1, LOGOUT);
        return this;
    }

    public AccountPage clickOnLogoutButton() {
        actions.element().click(LOGOUT);
        return this;
    }
}
