package com.swiggy.dineoutUI.pages.consumer.checkout;

import com.swiggy.automation.ui.locators.LocatorType;
import com.swiggy.dineoutUI.pages.SwiggyDineoutBasePage;
import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;

public class CheckoutPageLocators extends SwiggyDineoutBasePage<CheckoutPageLocators> {
    public By CASH_ON_DELIVERY = getLocator("CASH_ON_DELIVERY");
    public By PAY_WITH_CASH = getLocator("PAY_WITH_CASH");
    public By CANCEL_POLICY = getLocator("CANCEL_POLICY");
    public By PAYMENT_ALERT_CONTENT = getLocator("PAYMENT_ALERT_CONTENT");
    public By PAYMENT_ALERT_BUTTON = getLocator("PAYMENT_ALERT_BUTTON");

    @Override
    protected void setLocators() {
        setLocator("CASH_ON_DELIVERY", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.Button[@text=\"Cash Online payment recommended to reduce contact between you and delivery partner\"]"));
        setLocator("PAY_WITH_CASH", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.Button[@text=\"PAY WITH CASH\"]"));
        setLocator("CANCEL_POLICY", LocatorType.ANDROID_LOCATOR, MobileBy.id("button"));
        setLocator("PAYMENT_ALERT_CONTENT", LocatorType.ANDROID_LOCATOR, MobileBy.id("payment-alert-content"));
        setLocator("PAYMENT_ALERT_BUTTON", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.Button[starts-with(@text,\"PROCEED TO PAY\")]"));
    }
}
