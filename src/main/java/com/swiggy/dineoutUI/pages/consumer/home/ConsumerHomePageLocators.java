package com.swiggy.dineoutUI.pages.consumer.home;

import com.swiggy.automation.ui.locators.LocatorType;
import com.swiggy.dineoutUI.pages.SwiggyDineoutBasePage;
import org.openqa.selenium.By;

public class ConsumerHomePageLocators extends SwiggyDineoutBasePage<ConsumerHomePageLocators> {

    public By SWIGGY_ICON = getLocator("SWIGGY_ICON");
    public By SEARCH_ICON =  getLocator("SEARCH_ICON");
    public By CART_ICON = getLocator("CART_ICON");
    public By ACCOUNT_ICON = getLocator("ACCOUNT_ICON");
    public By LOCATION_DROPDOWN_ICON = getLocator("LOCATION_DROPDOWN_ICON");
    public By RESTAURANT_LIST  = getLocator("RESTAURANT_LIST");
    public By VIDEO_POP_UP_CLOSE_BTN = getLocator("VIDEO_POP_UP_CLOSE_BTN");
    public By ALL_RESTAURANTS = getLocator("ALL_RESTAURANTS");

    @Override
    protected void setLocators() {
        setLocator("SWIGGY_ICON", LocatorType.ANDROID_LOCATOR, By.id("bottom_bar_restaurant_title"));
        setLocator("SEARCH_ICON", LocatorType.ANDROID_LOCATOR, By.id("bottom_bar_explore_title"));
        setLocator("CART_ICON", LocatorType.ANDROID_LOCATOR, By.id("bottom_bar_cart_title"));
        setLocator("ACCOUNT_ICON", LocatorType.ANDROID_LOCATOR, By.id("bottom_bar_account_title"));
        setLocator("ALL_RESTAURANTS", LocatorType.ANDROID_LOCATOR, By.id("m_n_image_spec_id"));
        setLocator("LOCATION_DROPDOWN_ICON", LocatorType.ANDROID_LOCATOR, By.id("address_annotation_textview"));
        setLocator("VIDEO_POP_UP_CLOSE_BTN", LocatorType.ANDROID_LOCATOR, By.id("close_video_popup"));
        setLocator("RESTAURANT_LIST", LocatorType.ANDROID_LOCATOR, By.xpath("//android.widget.FrameLayout[contains(@resource-id,'iv_restaurant')]/.."));

        //      setLocator("ACCOUNT_ICON", LocatorType.ANDROID_LOCATOR,By.xpath("//android.widget.LinearLayout[contains(@resource-id,'bottom_bar_account')]"));
    }
}
