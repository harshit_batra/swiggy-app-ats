package com.swiggy.dineoutUI.pages.consumer.restaurants;

import com.swiggy.automation.ui.locators.LocatorType;
import com.swiggy.dineoutUI.pages.SwiggyDineoutBasePage;
import com.swiggy.dineoutUI.pages.vendor.login.RestaurantLoginPageLocators;
import io.appium.java_client.MobileBy;
import org.openqa.selenium.By;

public class RestaurantLocators extends SwiggyDineoutBasePage<RestaurantLoginPageLocators> {
    public By VENDOR_RESTAURANT = getLocator("VENDOR_RESTAURANT");
    public By RESTAURANT_LIST = getLocator("RESTAURANT_LIST");


    @Override
    protected void setLocators() {
        setLocator("VENDOR_RESTAURANT", LocatorType.ANDROID_LOCATOR, MobileBy.xpath("//android.widget.TextView[@text=\"Test Restaurant 1(Vendor) #\"]"));
        setLocator("RESTAURANT_LIST", LocatorType.ANDROID_LOCATOR, MobileBy.className("android.view.ViewGroup"));
    }
}
