package android;

import io.appium.java_client.MobileBy;
import io.appium.java_client.android.Activity;
import io.appium.java_client.android.AndroidDriver;
import io.appium.java_client.android.AndroidElement;
//import org.json.JSONArray;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;


public class SampleTest {

    public static void main(String[] args) throws MalformedURLException, InterruptedException {

        DesiredCapabilities caps = new DesiredCapabilities();

        // Set your access credentials
        caps.setCapability("browserstack.user", "harshitbatra_lqehZP");
        caps.setCapability("browserstack.key", "1wNuwdtpzux47AuJL5vZ");

        // Set URL of the application under test
    //    caps.setCapability("app", "harshitbatra_lqehZP/REApp");
        caps.setCapability("app", "harshitbatra_lqehZP/CEApp1");
//        JSONArray jsonArray = new JSONArray();
//
//        jsonArray.put("harshitbatra_lqehZP/CEApp");
//        System.out.println(jsonArray.toString());

        caps.setCapability("otherApps",new String[]{"harshitbatra_lqehZP/REApp"});

        // Specify device and os_version for testing
        caps.setCapability("device", "Google Pixel 3");
        caps.setCapability("os_version", "9.0");

        // Set other BrowserStack capabilities
        caps.setCapability("project", "First Java Project");
        caps.setCapability("build", "Java Android");
        caps.setCapability("name", "first_test");


        // Initialise the remote Webdriver using BrowserStack remote URL
        // and desired capabilities defined above
        AndroidDriver<AndroidElement> driver = new AndroidDriver<AndroidElement>(
                new URL("http://hub.browserstack.com/wd/hub"), caps);
        Thread.sleep(10000);

        AndroidElement loginCApp = driver.findElementById("item_menu_top_header_restaurant_name3");
        loginCApp.click();

        AndroidElement mobile = (AndroidElement) new WebDriverWait(driver, 30).until(
                ExpectedConditions.elementToBeClickable(
                        MobileBy.id("loginCheckPhoneNumberEditText")));
        mobile.sendKeys("9606494052");

        AndroidElement continueCApp = driver.findElementById("loginCheckButton");
        continueCApp.click();

        AndroidElement otp = (AndroidElement) new WebDriverWait(driver, 30).until(
                ExpectedConditions.elementToBeClickable(
                        MobileBy.id("otpField")));
        otp.sendKeys("086420");

        AndroidElement verifyCApp = driver.findElementById("forgotPasswordSubmitBtn");
        verifyCApp.click();



//        AndroidElement allowGPS = (AndroidElement) new WebDriverWait(driver, 30).until(
//                ExpectedConditions.elementToBeClickable(
//                        MobileBy.xpath("//android.widget.Button[contains(@resource-id,'permission_allow_button")));
//        allowGPS.click();
//
//        AndroidElement allowNot = (AndroidElement) new WebDriverWait(driver, 30).until(
//                ExpectedConditions.elementToBeClickable(
//                        MobileBy.xpath("//android.widget.Button[@text='OK']")));
//        allowNot.click();
//
//        AndroidElement accountIcon = (AndroidElement) new WebDriverWait(driver, 30).until(
//                ExpectedConditions.elementToBeClickable(
//                        MobileBy.id("bottom_bar_account_title")));

        Activity activity = new Activity("in.swiggy.partnerapp.uat", "in.swiggy.partnerapp.MainActivity");
        activity.setStopApp(false);
        driver.startActivity(activity);


        AndroidElement insertTextElement = (AndroidElement) new WebDriverWait(driver, 30).until(
                ExpectedConditions.elementToBeClickable(
                        MobileBy.className("android.widget.EditText")));
        insertTextElement.sendKeys("123");

        AndroidElement continueButton = driver.findElementByXPath("//android.widget.TextView[@text='CONTINUE']");
        continueButton.click();

        AndroidElement loginButton = (AndroidElement) new WebDriverWait(driver, 30).until(
                ExpectedConditions.elementToBeClickable(
                        MobileBy.xpath("//android.widget.TextView[@text='LOGIN']")));

        AndroidElement insertTextElement1 = (AndroidElement) new WebDriverWait(driver, 30).until(
                ExpectedConditions.elementToBeClickable(
                        MobileBy.className("android.widget.EditText")));
        insertTextElement1.sendKeys("456");

        loginButton.click();
        Thread.sleep(10000);
        try {
            activity = new Activity("in.swiggy.android", "in.swiggy.android.activities.HomeActivity");
            activity.setStopApp(false);
            driver.startActivity(activity);

            Thread.sleep(10000);
        }

        catch(Exception e) {
            System.out.println(e.getMessage());
        }
        finally {
            Thread.sleep(10000);
            activity = new Activity("in.swiggy.partnerapp.uat", "in.swiggy.partnerapp.MainActivity");
            activity.setStopApp(false);
            driver.startActivity(activity);
            Thread.sleep(10000);

        }



















//        AndroidElement insertTextElement = (AndroidElement) new WebDriverWait(driver, 30).until(
//                ExpectedConditions.elementToBeClickable(
//                        MobileBy.className("android.widget.EditText")));
//        insertTextElement.sendKeys("123");
//
//        AndroidElement continueButton = driver.findElementByXPath("//android.widget.TextView[@text='CONTINUE']");
//        continueButton.click();
//
//        AndroidElement loginButton = (AndroidElement) new WebDriverWait(driver, 30).until(
//                ExpectedConditions.elementToBeClickable(
//                        MobileBy.xpath("//android.widget.TextView[@text='LOGIN']")));
//
//        AndroidElement insertTextElement1 = (AndroidElement) new WebDriverWait(driver, 30).until(
//                ExpectedConditions.elementToBeClickable(
//                        MobileBy.className("android.widget.EditText")));
//        insertTextElement1.sendKeys("456");
//
//        loginButton.click();
//        Thread.sleep(10000);

//        AndroidElement businessText = (AndroidElement) new WebDriverWait(driver, 30).until(
//                ExpectedConditions.elementToBeClickable(
//                        MobileBy.xpath("//android.widget.TextView[@text='Business']")));

//        Activity activity = new Activity("in.swiggy.android", "in.swiggy.android.activities.HomeActivity");
//        try {
//            driver.startActivity(activity);
//        }
//        catch(Exception e) {
//            System.out.println(e.getMessage());
//        }
//        finally {
//            Thread.sleep(10000);
//            activity = new Activity("in.swiggy.partnerapp.uat", "in.swiggy.partnerapp.MainActivity");
//            activity.setStopApp(false);
//            driver.startActivity(activity);
//            Thread.sleep(10000);
//
//        }
//        Thread.sleep(10000);
//        activity = new Activity("in.swiggy.partnerapp.uat", "in.swiggy.partnerapp.MainActivity");
//        driver.startActivity(activity);
//        Thread.sleep(10000);






     //   driver.findElementByName("Login").click();
   //     driver.findElementById("in.swiggy.android:id/item_menu_top_header_restaurant_name3").click();

        // Test case for the BrowserStack sample Android app.
        // If you have uploaded your app, update the test case here.
//        AndroidElement searchElement = (AndroidElement) new WebDriverWait(driver, 30).until(
//                ExpectedConditions.elementToBeClickable(
//                        MobileBy.AccessibilityId("Search Wikipedia")));
//        searchElement.click();
//        AndroidElement insertTextElement = (AndroidElement) new WebDriverWait(driver, 30).until(
//                ExpectedConditions.elementToBeClickable(
//                        MobileBy.id("org.wikipedia.alpha:id/search_src_text")));
//        insertTextElement.sendKeys("BrowserStack");
//        Thread.sleep(5000);
//        List<AndroidElement> allProductsName = driver.findElementsByClassName(
//                "android.widget.TextView");
//        assert(allProductsName.size() > 0);


        // Invoke driver.quit() after the test is done to indicate that the test is completed.
        driver.quit();

    }

}