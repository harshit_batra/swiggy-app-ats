package com.swiggy.dineoutUI;

import com.swiggy.automation.ui.assertions.BaseTest;
import com.swiggy.automation.ui.enums.Environment;
import com.swiggy.automation.ui.helpers.CommonHelpers;
import com.swiggy.mocking.wiremock.WiremockListener;
import org.testng.annotations.Listeners;

@Listeners(WiremockListener.class)
public class BaseTestPage extends BaseTest {
    public Environment environment = CommonHelpers.getCurrentEnvironment();
}

