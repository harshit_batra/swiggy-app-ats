package com.swiggy.dineoutUI.NewOrder;

import com.swiggy.automation.ui.listeners.DriverCreationListener;
import com.swiggy.dineoutUI.BaseTestPage;
import com.swiggy.dineoutUI.assertions.consumer.ConsumerHomePageAssertions;
import com.swiggy.dineoutUI.assertions.consumer.ConsumerOrderConfirmationPageAssertions;
import com.swiggy.dineoutUI.assertions.vendor.VendorHomePageAssertions;
import com.swiggy.dineoutUI.constants.Consumer.ConsumerAppMetaData;
import com.swiggy.dineoutUI.constants.Vendor.VendorAppMetaData;
import com.swiggy.dineoutUI.helpers.CommonHelpers;
import com.swiggy.dineoutUI.helpers.consumer.ConsumerPageHelpers;
import com.swiggy.dineoutUI.helpers.vendor.VendorPageHelpers;
import com.swiggy.dineoutUI.DP.CommonDataProviders;
import com.swiggy.dineoutUI.pages.common.AppListingPage;
import com.swiggy.dineoutUI.pages.consumer.Cart.CartPage;
import com.swiggy.dineoutUI.pages.consumer.OrderConfirmation.OrderConfirmationPage;
import com.swiggy.dineoutUI.pages.consumer.RestaurantDetail.RestaurantDetailPage;
import com.swiggy.dineoutUI.pages.consumer.checkout.CheckoutPage;
import com.swiggy.dineoutUI.pages.consumer.home.ConsumerHomePage;
import com.swiggy.dineoutUI.pages.consumer.restaurants.RestaurantsPage;
import com.swiggy.dineoutUI.pages.vendor.home.VendorHomePage;
import com.swiggy.dineoutUI.pages.vendor.home.VendorHomePageLocators;
import com.swiggy.dineoutUI.pages.vendor.inventory.InventoryPage;
import com.swiggy.dineoutUI.pages.vendor.neworders.NewOrdersPage;
import com.swiggy.dineoutUI.pages.vendor.neworders.NewOrdersPageLocators;
import com.swiggy.dineoutUI.pages.vendor.orderdetail.OrderDetailLocators;
import com.swiggy.dineoutUI.pages.vendor.orderdetail.OrderDetailPage;
//import com.swiggy.generated.sources.cms.Restaurant;
//import com.swiggy.integration.factories.GlobalFactory;
import io.appium.java_client.android.AndroidDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;
import org.tinylog.Logger;

import java.net.MalformedURLException;

public class NewOrderNotificationTests extends BaseTestPage {

    VendorPageHelpers vendorPageHelpers = new VendorPageHelpers();
    ConsumerPageHelpers consumerPageHelpers = new ConsumerPageHelpers();
    String itemName, orderId, itemQuantity;

//    @BeforeSuite()
//    public void createRestaurantIfRequired() throws Exception {
//        Restaurant restaurant = null;
//        GlobalFactory globalFactory = new GlobalFactory();
//        globalFactory.createRestaurant(restaurant);
//    }

    @Test(description = "Should be able to login to vendor app", groups = {"PROD", "MOCKED", "UAT"})
    public void loginToVendorApp() throws InterruptedException {
        VendorHomePageAssertions vendorHomePageAssertions = new VendorHomePageAssertions();
        // Open partner app and Login

        AndroidDriver driver = (AndroidDriver) DriverCreationListener.driver.get();


        System.out.println("Deployment is " + System.getProperty("cap_deploy"));
        if(System.getProperty("cap_deploy").equals("PROD")) {
            CommonHelpers.openAnotherApp(VendorAppMetaData.packageName, VendorAppMetaData.activityName, VendorAppMetaData.appWaitPackageName, VendorAppMetaData.appWaitActivityName, false);
        }
        else if(System.getProperty("cap_deploy").equals("UAT")) {
            CommonHelpers.openAnotherApp(VendorAppMetaData.uatPackageName, VendorAppMetaData.uatActivityName, VendorAppMetaData.uatAppWaitPackageName, VendorAppMetaData.uarAppWaitActivityName, false);
        }

        VendorHomePage vendorHomePage = vendorPageHelpers.loginToVendorApp("8506861844", "El3ment@ry");
//        VendorHomePage vendorHomePage = vendorPageHelpers.loginToVendorApp("9738948943", "El3ment@ry");

        vendorHomePage.waitAndCloseHelpCentreText();
//      vendorHomePageAssertions.vendorHomePageAssertions(vendorHomePage);
        int currentOrders = vendorHomePage.getCurrentNewOrders();
        Logger.info("Current New Orders Are " + currentOrders);
    }

    @Test(description = "Should be able to login to consumer app", groups = {"PROD", "MOCKED", "UAT"}, dependsOnMethods = "loginToVendorApp")
    public void openConsumerAppAndLogin() throws MalformedURLException, InterruptedException {
        ConsumerHomePageAssertions consumerHomePageAssertions = new ConsumerHomePageAssertions();
        //Switch to Consumer App
        AppListingPage appListingPage = AppListingPage.getAppListingPage();
        appListingPage.clickOnSwitchAppButton();
        appListingPage.clickOnSwiggyApp();
        ConsumerHomePage consumerHomePage = consumerPageHelpers.loginToConsumerApp("9513267734", "086420");
        consumerHomePage.waitForVideoPopUpToBeVisible().clickOnVideoPopupCloseButton();
        consumerHomePageAssertions.consumerHomePageAssertions(consumerHomePage);
    }

    @Test(description = "Should be able to change location and see All Retaurants", groups = {"PROD", "MOCKED", "UAT"}, dependsOnMethods = "openConsumerAppAndLogin")
    public void changeLocationInConsumerApp() throws MalformedURLException, InterruptedException {
        ConsumerHomePageAssertions consumerHomePageAssertions = new ConsumerHomePageAssertions();
        ConsumerHomePage consumerHomePage = ConsumerHomePage.getConsumerHomePage();
        consumerPageHelpers.enterAndSelectLocation(consumerHomePage, "Budhwal");
        consumerHomePageAssertions.consumerHomePageAssertionsAfterSelectingLocation(consumerHomePage);
    }

    @Test(description = "Should be able to place order on consumer app", groups = {"PROD", "MOCKED", "UAT"}, dependsOnMethods = "changeLocationInConsumerApp")
    public void placeOrderOnConsumerApp() throws MalformedURLException, InterruptedException {
        ConsumerOrderConfirmationPageAssertions consumerOrderConfirmationPageAssertions = new ConsumerOrderConfirmationPageAssertions();
        ConsumerHomePage consumerHomePage=ConsumerHomePage.getConsumerHomePage();
        RestaurantsPage restaurantsPage = consumerHomePage.waitAndClickOnAllRestaurants();
        RestaurantDetailPage restaurantDetailPage = consumerPageHelpers.openVendorTestRestaurant(restaurantsPage);
        CartPage cartPage = consumerPageHelpers.addFirstItemToCart(restaurantDetailPage);
        itemName = cartPage.getCartItemName(0);
        itemQuantity = cartPage.getCartItemQuantity(0);
        Logger.info("Item Name Is " + itemName);
        Logger.info("Quantity Is " + itemQuantity);
        CheckoutPage checkoutPage = cartPage.waitAndClickOnProceedToPayButton();
        Thread.sleep(5000);
        checkoutPage.clickOnUnderstandCancelPolicy();
        OrderConfirmationPage orderConfirmationPage = consumerPageHelpers.orderUsingCashOnDelivery(checkoutPage);
        orderConfirmationPage.waitForOutletDetailsToBeDisplayed();
        Thread.sleep(5000);
        orderId = orderConfirmationPage.getVendorOrderId();
        NewOrdersPageLocators.orderId = orderId;
        VendorHomePageLocators.orderId = orderId;
        Logger.info("Vendor Order Id Is " + orderId);
        consumerOrderConfirmationPageAssertions.orderConfirmationAssertions(orderConfirmationPage);
//        orderConfirmationPage.clickOnHomeBackButton();
    }

    @Test(description = "Should be able to see placed order in vendor app", groups = {"PROD", "MOCKED", "UAT"}, dependsOnMethods = "placeOrderOnConsumerApp")
    public void switchToVendorAppToSeePlacedOrder() throws Exception {
        //Switch To Partner App After Placing Order
        AppListingPage appListingPage = AppListingPage.getAppListingPage();
        appListingPage.clickOnSwitchAppButton();
        appListingPage.clickOnPartnerApp();
        Thread.sleep(5000);
//      Comment this order id portion
//      orderId = "4158";
//      NewOrdersPageLocators.orderId = "4158";
//      Till here
        VendorHomePage vendorHomePage = VendorHomePage.getVendorHomePage();
        vendorHomePage.removeMinimizedConsumerAppFromScreen();
        int finalOrders = vendorHomePage.getCurrentNewOrders();
        OrderDetailPage orderDetailPage = vendorHomePage.clickOnNewOrders();
        orderDetailPage.clickOnOutOfStockNotification();
        orderDetailPage.waitForOrderIdToBeVisible();
        String vendorOrderId = orderDetailPage.getNewOrderIdFromOrderDetailPage();
        if(!vendorOrderId.contains(orderId)) {
            orderDetailPage.pressAndroidBackButton();
            NewOrdersPage newOrdersPage = NewOrdersPage.getNewOrdersPage();
            newOrdersPage.waitForPendingConfirmationTextToBeVisible();
            orderDetailPage = newOrdersPage.scrollAndClickOnOrderCard();
            orderDetailPage.clickOnOutOfStockNotification();
            orderDetailPage.waitForOrderIdToBeVisible();
            vendorOrderId = orderDetailPage.getNewOrderIdFromOrderDetailPage();
            Logger.info("Vendor Order Id " + vendorOrderId);
        }
//      Till here
//      Assert vendorOrderId contains orderId
//      Assert.assertEquals(currentOrders, finalOrders + 1, "New Order is not reflected at Vendor");
    }

    @Test(description = "Should be able to confirm order on vendor app", groups = {"PROD", "MOCKED", "UAT"}, dependsOnMethods = "switchToVendorAppToSeePlacedOrder")
    public void confirmPlacedOrder() throws Exception {
        OrderDetailPage orderDetailPage = OrderDetailPage.getOrderDetailsPage();
        orderDetailPage.confirmOrder();
        orderDetailPage.clickOnPrepTimeNotification();
        Thread.sleep(15000);    //Time required For Order To Be Confirmed
        orderDetailPage.goToOrdersPage();
    }
//
//    @Test(description = "Should be able to see Status as preparing in consumer app after confirming order", groups = {"PROD", "MOCKED", "UAT"}, dependsOnMethods = "confirmPlacedOrder")
//    public void checkPreparingStatusInConsumerApp() throws Exception {
//        //Switch To Consumer App
//        AppListingPage appListingPage = AppListingPage.getAppListingPage();
//        appListingPage.clickOnSwitchAppButton();
//        appListingPage.clickOnSwiggyApp();
//        OrderConfirmationPage orderConfirmationPage = OrderConfirmationPage.getOrderConfirmationPagePage();
//        orderConfirmationPage.waitForOutletDetailsToBeDisplayed();
//        String outletMessage = orderConfirmationPage.getOutletMessage();
//        Assert.assertTrue(outletMessage.contains("preparing"));
//    }
//
    @Test(description = "Should be able to mark food ready in vendor app", groups = {"PROD", "MOCKED", "UAT"}, dependsOnMethods = "confirmPlacedOrder")
    public void markFoodAsReadyInVendorApp() throws Exception {
        //Switch To Vendor App
        AppListingPage appListingPage = AppListingPage.getAppListingPage();
        appListingPage.clickOnSwitchAppButton();
        appListingPage.clickOnPartnerApp();
        VendorHomePage vendorHomePage = VendorHomePage.getVendorHomePage();
        vendorHomePage.removeMinimizedConsumerAppFromScreen();
        NewOrdersPage newOrdersPage = NewOrdersPage.getNewOrdersPage();
        OrderDetailPage orderDetailPage = newOrdersPage.scrollAndClickOnOrderCard();
        orderDetailPage.clickOnOutOfStockNotification();
        orderDetailPage.markFoodReady();
        Thread.sleep(10000);
        //Assertions
    }
}

