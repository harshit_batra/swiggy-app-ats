package com.swiggy.dineoutUI.DP;

import com.swiggy.automation.ui.helpers.CommonHelpers;
import com.swiggy.automation.ui.listeners.DriverCreationListener;
import com.swiggy.dineoutUI.BaseTestPage;
import lombok.extern.slf4j.Slf4j;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

@Slf4j
public class CommonDataProviders extends BaseTestPage {

    public static String MOCKED_APP_9091 = "anshalanand1/ANDROID_MOCK_STORES_9091";
    static Map<String, String> map = new HashMap<>();
    boolean isParallel = CommonHelpers.isParallel();

    public static synchronized String getNumber(String methodName) {
        String s = "";
        try {
            s = DriverCreationListener.getAvailableMobileNumbers();
            if (map.get(methodName) != null) {
                s = map.get(methodName);
            } else {
                map.put(methodName, s);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return !s.isEmpty() ? s : "6366956373";
    }

    @BeforeSuite
    public void beforeSuite() {
//		DriverCreationListener.setMobileNumbersInQueue();
    }

    @AfterSuite
    public void afterSuite() {
    }

    @DataProvider(name = "getCommonData")
    public synchronized Object[][] getCommonData(Method method) throws IOException {
            String methodName = method.getName();
            String s = CommonDataProviders.getNumber(methodName);
            return new Object[][]{{isParallel ? s : "9513267734",
                    "086420"}};
    }
}

//
//    @DataProvider(name = "getDataTestReorder")
//    public synchronized Object[][] getDataTestReorder(Method method) throws IOException {
//        if (environment.equals(Environment.MOCKED)) {
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.CHECKOUT_CART)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
//            return new Object[][]{{"6366956369",
//                    LoginConstants.PROD_OTP}};
//        }
//    }
//
//    @DataProvider(name = "getCommonOldData")
//    public synchronized Object[][] getCommonOldData(Method method) throws IOException {
//        if (environment.equals(Environment.MOCKED)) {
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.CHECKOUT_CART)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP}};
//        } else {
//            return new Object[][]{{LoginConstants.PROD_DEFAULT_MOBILE_NUMBER,
//                    LoginConstants.PROD_OTP}};
//        }
//    }
//
//    @DataProvider(name = "getPureVegData")
//    public synchronized Object[][] getPureVegData(Method method) throws IOException {
//        if (environment.equals(Environment.MOCKED)) {
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .cartFlowPage(CartFlowPage.VEG_CART_MENU)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, MOCKED_APP_9091}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
//            return new Object[][]{{isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER,
//                    LoginConstants.PROD_OTP}};
//        }
//    }
//
//    @DataProvider(name = "getCommonDataWithRestaurant")
//    public synchronized Object[][] getCommonDataWithRestaurant(Method method) throws IOException {
//
//        String restaurant = "Chai Point";
//        if (environment.equals(Environment.MOCKED)) {
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.CHECKOUT_CART)
//                    .searchPageType(SearchPageType.TRUFFLES)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, AddressConstants.HEALTHY_RESTAURANT_NAME}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
//            return new Object[][]{
//                    {isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER, LoginConstants.PROD_OTP, restaurant}
//            };
//        }
//    }
//
//    @DataProvider(name = "getCommonDataWithMenu")
//    public synchronized Object[][] getCommonDataWithMenu(Method method) throws IOException {
//        String menu = "Dosa";
//        if (environment.equals(Environment.MOCKED)) {
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.CHECKOUT_CART)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, menu}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
//            return new Object[][]{
//                    {isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER, LoginConstants.PROD_OTP, menu}
//            };
//        }
//    }
//
//    @DataProvider(name = "userData")
//    public synchronized Object[][] getAddNewAddressData(Method method) throws IOException {
//        if (environment.equals(Environment.MOCKED)) {
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.CHECKOUT_CART)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, AddressConstants.GPS_LOCATION, AddressConstants.HOUSE_NO,
//                    AddressConstants.LAND_MARK, AddressConstants.LOCATION_NAME}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
//
//            return new Object[][]{{isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER,
//                    LoginConstants.PROD_OTP, AddressConstants.GPS_LOCATION, AddressConstants.HOUSE_NO,
//                    AddressConstants.LAND_MARK, AddressConstants.LOCATION_NAME}};
//        }
//    }
//
//    @DataProvider(name = "getAddNewAddressDominosData")
//    public synchronized Object[][] getAddNewAddressDominosData(Method method) throws IOException {
//
//        if (environment.equals(Environment.MOCKED)) {
////			UserData userData = UserDataSetter
////					.setData(LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, AddressConstants.GPS_LOCATION, AddressConstants.HOUSE_NO,
////							AddressConstants.LAND_MARK, AddressConstants.LOCATION_NAME, AddressConstants.DOMINOS_RESTAURANT_NAME);
//            UserData userData = UserData.builder().mobile(LoginConstants.MOCK_MOBILE_NUMBER).otp(LoginConstants.MOCK_OTP)
//                    .location(AddressConstants.GPS_LOCATION).houseNo(AddressConstants.HOUSE_NO).landMark(AddressConstants.LAND_MARK)
//                    .locationName(AddressConstants.LOCATION_NAME).searchRestaurant(AddressConstants.RESTAURANT_NAME)
//                    .build();
//
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.DOMINOS_CART)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, AddressConstants.DOMINO_LOCATION, AddressConstants.DOMINOS_RESTAURANT_NAME, MOCKED_APP_9091}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
//			/*UserData userData = UserDataSetter
//					.setData(isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER,
//							LoginConstants.PROD_OTP, AddressConstants.GPS_LOCATION, AddressConstants.HOUSE_NO,
//							AddressConstants.LAND_MARK, AddressConstants.LOCATION_NAME, AddressConstants.DOMINOS_RESTAURANT_NAME);*/
//            UserData userData = UserData.builder().mobile(LoginConstants.PROD_DEFAULT_MOBILE_NUMBER).otp(LoginConstants.PROD_OTP)
//                    .location(AddressConstants.GPS_LOCATION).houseNo(AddressConstants.HOUSE_NO).landMark(AddressConstants.LAND_MARK)
//                    .locationName(AddressConstants.LOCATION_NAME).searchRestaurant(AddressConstants.RESTAURANT_NAME)
//                    .build();
//            return new Object[][]{{isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER,
//                    LoginConstants.PROD_OTP, AddressConstants.DOMINO_LOCATION, AddressConstants.DOMINOS_RESTAURANT_NAME}};
//        }
//    }
//
//    @DataProvider(name = "getSearchLocationData")
//    public synchronized Object[][] getSearchLocationData(Method method) throws IOException {
//        if (environment.equals(Environment.MOCKED)) {
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.CHECKOUT_CART)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, AddressConstants.GPS_DISABLED_LOCATION}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
//            return new Object[][]{
//                    {isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER,
//                            LoginConstants.PROD_OTP, AddressConstants.GPS_DISABLED_LOCATION}
//            };
//        }
//    }
//
//    @DataProvider(name = "getSearchLocationOldData")
//    public synchronized Object[][] getSearchLocationOldData(Method method) throws IOException {
//        if (environment.equals(Environment.MOCKED)) {
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.CHECKOUT_CART)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, AddressConstants.GPS_DISABLED_LOCATION}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
//            return new Object[][]{
//                    {isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER,
//                            LoginConstants.PROD_OTP, AddressConstants.GPS_DISABLED_LOCATION}
//            };
//        }
//    }
//
//
//    @DataProvider(name = "getAddNewAddressDataWithoutGPSLocation")
//    public synchronized Object[][] getAddNewAddressDataWithoutGPSLocation(Method method) throws IOException {
//        String houseFlatNo = "123";
//        String landmark = "123";
//        String addressTitle = "123";
//        if (environment.equals(Environment.MOCKED)) {
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.CHECKOUT_CART)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, houseFlatNo, landmark, addressTitle}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
//            return new Object[][]{{isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER,
//                    LoginConstants.PROD_OTP, houseFlatNo, landmark, addressTitle}};
//        }
//    }
//
//    @DataProvider(name = "getCommonDataWithHealthyRestaurant")
//    public synchronized Object[][] getCommonDataWithHealthyRestaurant(Method method) throws IOException {
//
//        String restaurant = "Truffles";
//        if (environment.equals(Environment.MOCKED)) {
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.CHECKOUT_CART)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, restaurant}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
//            return new Object[][]{
//                    {isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER, LoginConstants.PROD_OTP, restaurant}
//            };
//        }
//    }
//
//    @DataProvider(name = "getCommonDataWithHygieneRestaurant")
//    public synchronized Object[][] getCommonDataWithHygieneRestaurant(Method method) throws IOException {
//        String restaurant = "Wat-A-Burger!";
//        String location = "DLF Phase 4,Gurugram,Haryana";
//        if (environment.equals(Environment.MOCKED)) {
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.CHECKOUT_CART)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, location, restaurant}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
//            return new Object[][]{
//                    {isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER, LoginConstants.PROD_OTP, location, restaurant}
//            };
//        }
//    }
//
//    @DataProvider(name = "getCommonDataWithUnserviceableRestaurant")
//    public synchronized Object[][] getCommonDataWithUnserviceableRestaurant(Method method) throws IOException {
//        String restaurant = "Twilight Express";
//        if (environment.equals(Environment.MOCKED)) {
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.CHECKOUT_CART)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, restaurant}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
//            return new Object[][]{
//                    {isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER, LoginConstants.PROD_OTP, restaurant}
//            };
//        }
//    }
//
//    @DataProvider(name = "getAddNewAddressDataHealthy")
//    public synchronized Object[][] getAddNewAddressDataHealthy(Method method) throws IOException {
//
//        if (environment.equals(Environment.MOCKED)) {
//			/*UserData userData = UserDataSetter
//					.setData(LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, AddressConstants.GPS_LOCATION, AddressConstants.HOUSE_NO,
//							AddressConstants.LAND_MARK, AddressConstants.LOCATION_NAME, AddressConstants.HEALTHY_RESTAURANT_NAME);*/
//            UserData userData = UserData.builder().mobile(LoginConstants.MOCK_MOBILE_NUMBER).otp(LoginConstants.MOCK_OTP)
//                    .location(AddressConstants.GPS_LOCATION).houseNo(AddressConstants.HOUSE_NO).landMark(AddressConstants.LAND_MARK)
//                    .locationName(AddressConstants.LOCATION_NAME).searchRestaurant(AddressConstants.HEALTHY_RESTAURANT_NAME)
//                    .build();
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.CHECKOUT_CART)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, AddressConstants.HEALTHY_RESTAURANT_NAME}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
////			UserData userData = UserDataSetter
////					.setData(isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER,
////							LoginConstants.PROD_OTP, AddressConstants.GPS_LOCATION, AddressConstants.HOUSE_NO,
////							AddressConstants.LAND_MARK, AddressConstants.LOCATION_NAME, AddressConstants.HEALTHY_RESTAURANT_NAME);
//            UserData userData = UserData.builder().mobile(LoginConstants.PROD_DEFAULT_MOBILE_NUMBER).otp(LoginConstants.PROD_OTP)
//                    .location(AddressConstants.GPS_LOCATION).houseNo(AddressConstants.HOUSE_NO).landMark(AddressConstants.LAND_MARK)
//                    .locationName(AddressConstants.LOCATION_NAME).searchRestaurant(AddressConstants.HEALTHY_RESTAURANT_NAME)
//                    .build();
//            return new Object[][]{{isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER,
//                    LoginConstants.PROD_OTP, AddressConstants.HEALTHY_RESTAURANT_NAME}};
//        }
//    }
//
//    @DataProvider(name = "getMenuTDAndudgeData")
//    public synchronized Object[][] getMenuTDAndudgeData(Method method) throws IOException {
//
//        if (environment.equals(Environment.MOCKED)) {
//			/*UserData userData = UserDataSetter
//					.setData(LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, AddressConstants.GPS_LOCATION, AddressConstants.HOUSE_NO,
//							AddressConstants.LAND_MARK, AddressConstants.LOCATION_NAME, AddressConstants.HEALTHY_RESTAURANT_NAME);
//*/
//            UserData userData = UserData.builder().mobile(LoginConstants.MOCK_MOBILE_NUMBER).otp(LoginConstants.MOCK_OTP)
//                    .location(AddressConstants.GPS_LOCATION).houseNo(AddressConstants.HOUSE_NO).landMark(AddressConstants.LAND_MARK)
//                    .locationName(AddressConstants.LOCATION_NAME).searchRestaurant(AddressConstants.HEALTHY_RESTAURANT_NAME)
//                    .build();
//
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.CHECKOUT_CART)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, AddressConstants.HEALTHY_RESTAURANT_NAME}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
////			UserData userData = UserDataSetter
////					.setData(isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER,
////							LoginConstants.PROD_OTP, AddressConstants.GPS_LOCATION, AddressConstants.HOUSE_NO,
////							AddressConstants.LAND_MARK, AddressConstants.LOCATION_NAME, AddressConstants.HEALTHY_RESTAURANT_NAME);
//            UserData userData = UserData.builder().mobile(LoginConstants.PROD_DEFAULT_MOBILE_NUMBER).otp(LoginConstants.PROD_OTP)
//                    .location(AddressConstants.GPS_LOCATION).houseNo(AddressConstants.HOUSE_NO).landMark(AddressConstants.LAND_MARK)
//                    .locationName(AddressConstants.LOCATION_NAME).searchRestaurant(AddressConstants.TD_NUDGE_RESTAURANT_NAME)
//                    .build();
//            return new Object[][]{{isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER,
//                    LoginConstants.PROD_OTP, AddressConstants.TD_NUDGE_RESTAURANT_NAME}};
//        }
//    }
//
//    @DataProvider(name = "getCommonDataWithNetBanking")
//    public synchronized Object[][] getCommonDataWithNetBanking(Method method) throws IOException {
//
//        if (environment.equals(Environment.MOCKED)) {
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.CHECKOUT_CART)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, PaymentConstants.NET_BANKING}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
//            return new Object[][]{
//                    {isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER,
//                            LoginConstants.PROD_OTP, PaymentConstants.NET_BANKING}
//            };
//        }
//    }
//
//    @DataProvider(name = "getCommonDataWithAddCard")
//    public synchronized Object[][] getCommonDataWithAddCard(Method method) throws IOException {
//
//        if (environment.equals(Environment.MOCKED)) {
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.CHECKOUT_CART)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP,
//                    PaymentConstants.CARD_NUMBER, PaymentConstants.CARD_EXPIRY, PaymentConstants.CARD_CVV, PaymentConstants.CARD_USER_NAME}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
//            return new Object[][]{
//                    {isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER,
//                            LoginConstants.PROD_OTP, PaymentConstants.CARD_NUMBER, PaymentConstants.CARD_EXPIRY,
//                            PaymentConstants.CARD_CVV, PaymentConstants.CARD_USER_NAME}
//            };
//        }
//    }
//
//    @DataProvider(name = "Dominos Restaurant Data")
//    public synchronized Object[][] getDominosFlowData(Method method) throws IOException {
//        String restaurant = "Domino's Pizza";
//        if (environment.equals(Environment.MOCKED)) {
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName("Automation")
//                    .discount(73)
//                    .cartFlowPage(CartFlowPage.DOMINOS_CART)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, AddressConstants.DOMINO_LOCATION, restaurant, MOCKED_APP_9091}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
//            return new Object[][]{{isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER,
//                    LoginConstants.PROD_OTP, AddressConstants.DOMINO_LOCATION, restaurant}};
//        }
//    }
//
//
//    @DataProvider(name = "Liquor Flow data")
//    public synchronized Object[][] getLiquorFlowData(Method method) throws IOException {
//        if (environment.equals(Environment.MOCKED)) {
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.CHECKOUT_CART)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, AddressConstants.LIQUOR_LOCATION, AddressConstants.LIQUOR_AREA, AddressConstants.LIQUOR_HOUSE_NO
//                    , AddressConstants.LIQUOR_LANDMARK, AddressConstants.LIQUOR_ADDRESS_TITLE}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
//            return new Object[][]{{isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER,
//                    LoginConstants.PROD_OTP, AddressConstants.LIQUOR_LOCATION, AddressConstants.LIQUOR_AREA, AddressConstants.LIQUOR_HOUSE_NO
//                    , AddressConstants.LIQUOR_LANDMARK, AddressConstants.LIQUOR_ADDRESS_TITLE}};
//        }
//    }
//
//    @DataProvider(name = "getLiquorFlowOldData")
//    public synchronized Object[][] getLiquorFlowOldData(Method method) throws IOException {
//        if (environment.equals(Environment.MOCKED)) {
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.CHECKOUT_CART)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, AddressConstants.LIQUOR_LOCATION, AddressConstants.LIQUOR_AREA, AddressConstants.LIQUOR_HOUSE_NO
//                    , AddressConstants.LIQUOR_LANDMARK, AddressConstants.LIQUOR_ADDRESS_TITLE}};
//        } else {
//            return new Object[][]{{LoginConstants.PROD_DEFAULT_MOBILE_NUMBER,
//                    LoginConstants.PROD_OTP, AddressConstants.LIQUOR_LOCATION, AddressConstants.LIQUOR_AREA, AddressConstants.LIQUOR_HOUSE_NO
//                    , AddressConstants.LIQUOR_LANDMARK, AddressConstants.LIQUOR_ADDRESS_TITLE}};
//        }
//    }
//
//    @DataProvider(name = "Liquor Netbanking Flow data")
//    public synchronized Object[][] getLiquorNetbankingFlowData(Method method) throws IOException {
//        if (environment.equals(Environment.MOCKED)) {
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.CHECKOUT_CART)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, AddressConstants.LIQUOR_LOCATION, AddressConstants.LIQUOR_AREA, AddressConstants.LIQUOR_HOUSE_NO
//                    , AddressConstants.LIQUOR_LANDMARK, AddressConstants.LIQUOR_ADDRESS_TITLE, PaymentConstants.NET_BANKING}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
//            return new Object[][]{{isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER,
//                    LoginConstants.PROD_OTP, AddressConstants.LIQUOR_LOCATION, AddressConstants.LIQUOR_AREA, AddressConstants.LIQUOR_HOUSE_NO
//                    , AddressConstants.LIQUOR_LANDMARK, AddressConstants.LIQUOR_ADDRESS_TITLE, PaymentConstants.NET_BANKING}};
//        }
//    }
//
//
//    @DataProvider(name = "Liquor AddCard Flow data")
//    public synchronized Object[][] getLiquorAddCardFlowData(Method method) throws IOException {
//
//        if (environment.equals(Environment.MOCKED)) {
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.CHECKOUT_CART)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, AddressConstants.LIQUOR_LOCATION,
//                    PaymentConstants.CARD_NUMBER, PaymentConstants.CARD_EXPIRY, PaymentConstants.CARD_CVV, PaymentConstants.CARD_USER_NAME}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
//            return new Object[][]{
//                    {isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER,
//                            LoginConstants.PROD_OTP, AddressConstants.LIQUOR_LOCATION, PaymentConstants.CARD_NUMBER, PaymentConstants.CARD_EXPIRY,
//                            PaymentConstants.CARD_CVV, PaymentConstants.CARD_USER_NAME}
//            };
//        }
//    }
//
//    @DataProvider(name = "couponData")
//    public synchronized Object[][] getCouponData(Method method) throws IOException {
//
//        String restaurant = "Truffle";
//        if (environment.equals(Environment.MOCKED)) {
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.CHECKOUT_CART)
//                    .searchPageType(SearchPageType.TRUFFLES)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, restaurant}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
////			UserData userData = UserDataSetter
////					.setData(isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER,
////							LoginConstants.PROD_OTP, AddressConstants.GPS_LOCATION, AddressConstants.HOUSE_NO,
////							AddressConstants.LAND_MARK, AddressConstants.LOCATION_NAME, restaurant);
//
//            UserData userData = UserData.builder().mobile(LoginConstants.PROD_DEFAULT_MOBILE_NUMBER).otp(LoginConstants.PROD_OTP)
//                    .location(AddressConstants.GPS_LOCATION).houseNo(AddressConstants.HOUSE_NO).landMark(AddressConstants.LAND_MARK)
//                    .locationName(AddressConstants.LOCATION_NAME).searchRestaurant(restaurant)
//                    .build();
//
//            return new Object[][]{{isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER,
//                    LoginConstants.PROD_OTP, restaurant}};
//
//        }
//    }
//
//    @DataProvider(name = "getListingCollectionsdata")
//    public synchronized Object[][] getListingCollectionsData(Method method) throws IOException {
//        String restaurantName = "Truffles";
//        try {
//            String restaurant = System.getenv("Mechandized_Restaurant_Name");
//            if ((!restaurant.isEmpty()) && (restaurant != null)) {
//                restaurantName = restaurant;
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//
//        if (environment.equals(Environment.MOCKED)) {
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.DOMINOS_CART)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, restaurantName, MOCKED_APP_9091}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
//            return new Object[][]{
//                    {isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER, LoginConstants.PROD_OTP, restaurantName}
//            };
//        }
//    }
//
//    @DataProvider(name = "getFreebieRestaurantData")
//    public synchronized Object[][] getFreebieRestaurantData(Method method) throws IOException {
//        String location = "Dlf phase 4";
//        String mobileNumber;
//        String otp;
//        String houseFlatNo = "Testing location";
//        String landmark = "sample location";
//        String addressTitle = "Freebie location";
//        if (environment.equals(Environment.MOCKED)) {
//            location = "Dl";
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.CHECKOUT_CART)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, location, houseFlatNo, landmark, addressTitle}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
//            return new Object[][]{
//                    {isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER, LoginConstants.PROD_OTP, location, houseFlatNo, landmark, addressTitle}
//            };
//        }
//    }
//
//    @DataProvider(name = "getOneVariantData")
//    public synchronized Object[][] getOneVariantData(Method method) throws IOException {
//        String menu = "Tandoori Chicken";
//        if (environment.equals(Environment.MOCKED)) {
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.CHECKOUT_CART)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, menu}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
//            return new Object[][]{
//                    {isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER, LoginConstants.PROD_OTP, menu}};
//        }
//    }
//
//
//    @DataProvider(name = "getVariantsAndAddOnsData")
//    public synchronized Object[][] getCustomMenuWithVariantsAndAddOnsData(Method method) throws IOException {
//        String restaurant = "Subway";
//        String addOnSectionTittle = "Add On";
//        if (environment.equals(Environment.MOCKED)) {
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.SUBWAY_CART)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, restaurant, addOnSectionTittle, MOCKED_APP_9091}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
//            return new Object[][]{
//                    {isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER, LoginConstants.PROD_OTP, restaurant, addOnSectionTittle}};
//        }
//    }
//
//    @DataProvider(name = "getAddOnsOnlyData")
//    public synchronized Object[][] getAddOnsOnly(Method method) throws IOException {
//        String restaurant = "Meghana Foods";
//        if (environment.equals(Environment.MOCKED)) {
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, restaurant, MOCKED_APP_9091}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
//            return new Object[][]{
//                    {isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER, LoginConstants.PROD_OTP, restaurant}};
//        }
//    }
//
//    @DataProvider(name = "getMultipleVariantData")
//    public synchronized Object[][] getMultipleVariantData(Method method) throws IOException {
//        String restaurant = "Domino's Pizza";
//        if (environment.equals(Environment.MOCKED)) {
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.DOMINOS_CART)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, AddressConstants.DOMINO_LOCATION, restaurant, MOCKED_APP_9091}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
//            return new Object[][]{
//                    {isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER, LoginConstants.PROD_OTP, AddressConstants.DOMINO_LOCATION, restaurant}};
//        }
//    }
//
//    @DataProvider(name = "getCommonDataWithVegOnlyRestaurant")
//    public synchronized Object[][] getCommonDataWithVegOnlyRestaurant(Method method) throws IOException {
//        String restaurant = "Chai Point";
//        String item = "Chicken";
//        if (environment.equals(Environment.MOCKED)) {
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.CHECKOUT_CART)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, restaurant, item}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
//            return new Object[][]{
//                    {isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER, LoginConstants.PROD_OTP, restaurant, item}
//            };
//        }
//    }
//
//    @DataProvider(name = "getOrderFlowData")
//    public synchronized Object[][] getOrderFlowData(Method method) throws IOException {
//        UserData userData = UserData.builder()
//                .location(AddressConstants.ORDER_LOCATION).searchRestaurant(AddressConstants.ORDER_RESTAURANT_NAME)
//                .houseNo(AddressConstants.ORDER_HOUSE_NO).landMark(AddressConstants.ORDER_LAND_MARK)
//                .locationName(AddressConstants.ORDER_LOCATION_NAME)
//                .build();
//        if (environment.equals(Environment.MOCKED)) {
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.CHECKOUT_CART)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, AddressConstants.ORDER_LOCATION, AddressConstants.ORDER_RESTAURANT_NAME,
//                    AddressConstants.ORDER_HOUSE_NO, AddressConstants.ORDER_LAND_MARK, AddressConstants.ORDER_LOCATION_NAME}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
//            return new Object[][]{{LoginConstants.PROD_ORDER_FLOW_MOBILE_NUMBER,
//                    LoginConstants.PROD_OTP, AddressConstants.ORDER_LOCATION, AddressConstants.ORDER_RESTAURANT_NAME,
//                    AddressConstants.ORDER_HOUSE_NO, AddressConstants.ORDER_LAND_MARK, AddressConstants.ORDER_LOCATION_NAME}};
//        }
//    }
//
//    @DataProvider(name = "getCommonDataWithDish")
//    public synchronized Object[][] getCommonDataWithDish(Method method) throws IOException {
//
//        String dish = "Pizza";
//        if (environment.equals(Environment.MOCKED)) {
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.CHECKOUT_CART)
//                    .searchPageType(SearchPageType.TRUFFLES)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, dish}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
//            return new Object[][]{
//                    {isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER, LoginConstants.PROD_OTP, dish}
//            };
//        }
//
//    }
//
//    @DataProvider(name = "getCommonDataWithWrongDishName")
//    public synchronized Object[][] getCommonDataWithWrongDishName(Method method) throws IOException {
//
//        String dish = "Chikn";
//        if (environment.equals(Environment.MOCKED)) {
//            MockHelpers mockHelpers = MockHelpers.builder()
//                    .couponName(CouponCostants.COUPON_NAME)
//                    .discount(CouponCostants.DISCOUNT_VAL)
//                    .cartFlowPage(CartFlowPage.CHECKOUT_CART)
//                    .searchPageType(SearchPageType.TRUFFLES)
//                    .build();
//            mockHelpers.mockFromLoginToPaymentNetbankingResponse();
//            return new Object[][]{{LoginConstants.MOCK_MOBILE_NUMBER, LoginConstants.MOCK_OTP, dish}};
//        } else {
//            String methodName = method.getName();
//            String s = CommonDataProviders.getNumber(methodName);
//            return new Object[][]{
//                    {isParallel ? s : LoginConstants.PROD_DEFAULT_MOBILE_NUMBER, LoginConstants.PROD_OTP, dish}
//            };
//        }
//    }
